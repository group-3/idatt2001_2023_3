package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.ItemMissingException;
import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryActionTest {
  @Test
  void execute_add_item_to_inventory() throws PlayerMissingException, ItemMissingException {
    //Arrange
    String item = "Ball";
    Player player = new Player.PlayerBuilder("Test").build();
    InventoryAction inventoryAction = new InventoryAction(item);
    //Act
    inventoryAction.execute(player);
    //Assert
    assertTrue(player.getInventory().contains(item));
  }

  @Test
  void execute_player_null_throws_exception(){
    //Arrange
    String item = "Ball";
    InventoryAction inventoryAction = new InventoryAction(item);
    Player player = null;
    //Act and Assert
    assertThrows(PlayerMissingException.class, () -> inventoryAction.execute(player));
  }
}