package edu.ntnu.idatt2001.paths.story;

import edu.ntnu.idatt2001.paths.actions.Action;
import edu.ntnu.idatt2001.paths.actions.GoldAction;
import edu.ntnu.idatt2001.paths.exceptions.ActionMissingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkTest {
  @Test
  void addAction_add_action() throws ActionMissingException {
  //Arrange
  int gold = 5;
  String text = "Test";
  String referece= "agfa12";
  Action action = new GoldAction(gold);
  Link link = new Link(text,referece);
  //Act
  link.addAction(action);
  //Assert
  assertTrue(link.getActions().contains(action));
  }

  @Test
  void addAction_add_null() {
    //Arrange
    String text = "Test";
    String referece= "agfa12";
    Action action = null;
    Link link = new Link(text,referece);
    //Act
    //Assert
    assertThrows(ActionMissingException.class, () -> link.addAction(action));
  }

  @Test
  void testEquals_with_null() {
    //Arrange
    String text = "Test";
    String reference = "adt123";
    Link link = new Link(text, reference);
    //Act
    Boolean isEqual = link.equals(null);
    //Assert
    assertFalse(isEqual);
  }

  @Test
  void testEquals_with_string() {
    //Arrange
    String text = "Test";
    String reference = "adt123";
    Link link = new Link(text, reference);
    //Act
    Boolean isEqual = link.equals("Hello");
    //Assert
    assertFalse(isEqual);
  }

  @Test
  void testEquals_with_non_equal_link() {
    //Arrange
    String text = "Test";
    String reference = "adt123";
    Link link = new Link(text, reference);
    Link notEqualLink = new Link(text, "ghj43");
    //Act
    Boolean isEqual = link.equals(notEqualLink);
    //Assert
    assertFalse(isEqual);
  }

  @Test
  void testEquals_with_two_equal_objects() {
    //Arrange
    String text = "Test";
    String reference = "adt123";
    Link link = new Link(text, reference);
    //Act
    Boolean isEqual = link.equals(link);
    //Assert
    assertTrue(isEqual);
  }

  @Test
  void testEquals_with_equal_link() {
    //Arrange
    String text = "Test";
    String reference = "adt123";
    Link link = new Link(text, reference);
    Link equalLink = new Link("notEqual", reference);
    //Act
    Boolean isEqual = link.equals(equalLink);
    //Assert
    assertTrue(isEqual);
  }

}