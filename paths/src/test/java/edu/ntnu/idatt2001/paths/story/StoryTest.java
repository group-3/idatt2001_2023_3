package edu.ntnu.idatt2001.paths.story;

import edu.ntnu.idatt2001.paths.exceptions.*;
import edu.ntnu.idatt2001.paths.fileHandling.StoryFileReader;
import edu.ntnu.idatt2001.paths.fileHandling.StoryFileWriter;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StoryTest {

  @Test
  void addPassage() throws PassageMissingException {
    //Arrange
    String text = "Test";
    String title = "Passage";
    Passage passage = new Passage(title, text);
    Passage openingPassage = new Passage("opening", text);
    Story story = new Story("storyTest",openingPassage);
    //Act
    story.addPassage(passage);
    //Assert
    assertTrue(story.getPassages().contains(passage));
  }

  @Test
  void addPassage_if_passage_is_null() throws PassageMissingException {
    //Arrange
    String text = "Test";
    Passage passage = null;
    Passage openingPassage = new Passage("opening", text);
    Story story = new Story("storyTest",openingPassage);
    //Act
    //Assert
    assertThrows(PassageMissingException.class, ()->story.addPassage(passage));
  }

  @Test
  void getPassages_() throws PassageMissingException {
    //Arrange
    String title = "test";
    String content = "passage";
    Passage openingPassage = new Passage(title, content);
    Story story = new Story("storyTest", openingPassage);

    //Act
    List<Passage> result = story.getPassages().stream().toList();

    //Assert
    assertEquals(1, result.size());
    assertEquals(title, result.get(0).getTitle());
    assertEquals(content, result.get(0).getContent());
  }

  @Test
  void get_broken_links_two_broken_links() throws ReaderProblemException, PassageMissingException, ActionMissingException, FileFormatException, LinkMissingException {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another door
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);
    Story story = reader.createStoryFromFile(file);

    assertEquals(2, story.getBrokenLinks().size());
  }

  @Test
  void get_broken_links_zero_broken_links() throws ReaderProblemException, PassageMissingException, ActionMissingException, FileFormatException, LinkMissingException {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);
    Story story = reader.createStoryFromFile(file);

    assertEquals(0, story.getBrokenLinks().size());
  }

  @Test
  void remove_passage_throws_broken_links_exception() throws ReaderProblemException, PassageMissingException, ActionMissingException, FileFormatException, LinkMissingException {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);
    Story story = reader.createStoryFromFile(file);
    Link link = new Link("Try to open the door", "Another room");

    assertThrows(IllegalArgumentException.class, () -> story.removePassage(link));
  }

  @Test
  void remove_passage() throws ReaderProblemException, PassageMissingException, ActionMissingException, FileFormatException, LinkMissingException {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)
                
                ::Flip light switch
                The room lights up, and you realise your mistake when you stand face to face with a giant spider.
                [Run back!](Beginnings)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);
    Story story = reader.createStoryFromFile(file);
    Link link = new Link("Flip light switch", "Flip light switch");
    story.removePassage(link);

    assertEquals(2,story.getPassages().size());
  }

}