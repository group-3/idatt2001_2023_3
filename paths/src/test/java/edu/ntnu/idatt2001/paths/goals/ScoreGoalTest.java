package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.game.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScoreGoalTest {

    @Test
    void isFulfilled_has_fulfilled_score_goal_over() {
      //Arrange
      int score = 14;
      int scoreAmountGoal = 12;
      Player player = new Player.PlayerBuilder("Test").score(score).build();
      ScoreGoal scoreGoal = new ScoreGoal(scoreAmountGoal);
      //Act
      //Assert
      assertTrue(scoreGoal.isFulfilled(player));
    }

    @Test
    void isFulfilled_has_fulfilled_score_goal_exactly() {
      //Arrange
      int score = 12;
      int scoreAmountGoal = 12;
      Player player = new Player.PlayerBuilder("Test").score(score).build();
      ScoreGoal scoreGoal = new ScoreGoal(scoreAmountGoal);
      //Act
      //Assert
      assertTrue(scoreGoal.isFulfilled(player));
    }

    @Test
    void isFulfilled_has_not_fulfilled_score_goal() {
      //Arrange
      int score = 10;
      int scoreAmountGoal = 12;
        Player player = new Player.PlayerBuilder("Test").score(score).build();
      ScoreGoal scoreGoal = new ScoreGoal(scoreAmountGoal);
      //Act
      //Assert
      assertFalse(scoreGoal.isFulfilled(player));
    }

    @Test
    void isFulfilled_has_negative_minimum_score_() throws IllegalArgumentException {
      //Arrange
      int scoreAmountGoal = -5;
      //Act
      //Assert
      assertThrows(IllegalArgumentException.class, ()->new GoldGoal(scoreAmountGoal));
    }
}