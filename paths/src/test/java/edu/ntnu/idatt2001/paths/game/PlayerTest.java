package edu.ntnu.idatt2001.paths.game;

import edu.ntnu.idatt2001.paths.actions.InventoryAction;
import edu.ntnu.idatt2001.paths.exceptions.ItemMissingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

  @Test
  void addHealth_positive_number() {
    Player player = new Player.PlayerBuilder("Test").health(100).build();
    int healthToAdd = 10;
    player.addHealth(healthToAdd);
    assertEquals(110,player.getHealth());
  }

  @Test
  void addHealth_negative_number() {
    Player player = new Player.PlayerBuilder("Test").health(100).build();
    int healthToAdd = -10;
    player.addHealth(healthToAdd);
    assertEquals(90,player.getHealth());
  }

  @Test
  void addHealth_null() {
    Player player = new Player.PlayerBuilder("Test").health(100).build();
    int healthToAdd = 0;
    player.addHealth(healthToAdd);
    assertEquals(100,player.getHealth());
  }

  @Test
  void addScore_positive_number() {
    Player player = new Player.PlayerBuilder("Test").score(10).build();
    int scoreToAdd = 10;
    player.addScore(scoreToAdd);
    assertEquals(20, player.getScore());
  }

  @Test
  void addScore_negative_number() {
    Player player = new Player.PlayerBuilder("Test").score(10).build();
    int scoreToAdd = -10;
    player.addScore(scoreToAdd);
    assertEquals(0, player.getScore());
  }

  @Test
  void addScore_add_zero() {
    Player player = new Player.PlayerBuilder("Test").score(10).build();
    int scoreToAdd = 0;
    player.addScore(scoreToAdd);
    assertEquals(10, player.getScore());
  }

  @Test
  void addGold_positive_number() {
    Player player = new Player.PlayerBuilder("Test").gold(1).build();
    int goldToAdd = 10;
    player.addGold(goldToAdd);
    assertEquals(11,player.getGold());
  }

  @Test
  void addGold_negative_number() {
    Player player = new Player.PlayerBuilder("Test").gold(12).build();
    int goldToAdd = -10;
    player.addGold(goldToAdd);
    assertEquals(2,player.getGold());
  }

  @Test
  void addGold_zero() {
    Player player = new Player.PlayerBuilder("Test").gold(1).build();
    int goldToAdd = 0;
    player.addGold(goldToAdd);
    assertEquals(1,player.getGold());
  }

  @Test
  void addToInventory_string() throws ItemMissingException {
    Player player = new Player.PlayerBuilder("Test").build();
    String itemToAdd = "test";
    player.addToInventory(itemToAdd);
    assertTrue(player.getInventory().contains(itemToAdd));
  }

  @Test
  void execute_null_item_to_add_to_inventory() {
    //Arrange
    String text = null;
    Player player = new Player.PlayerBuilder("Test").build();
    InventoryAction inventoryAction = new InventoryAction(text);
    //Act
    //Assert
    assertThrows(ItemMissingException.class, () -> inventoryAction.execute(player), "The item cant be null");
  }

}