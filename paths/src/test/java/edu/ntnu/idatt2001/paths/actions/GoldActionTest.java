package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//Run ..... with coverage
//Trenger ikke teste disse når jeg har testen add i player

class GoldActionTest {

  @Test
  void execute_positive_value_is_added_to_player_gold() throws PlayerMissingException {
    //Arrange
    int gold = 100;
    GoldAction goldAction = new GoldAction(gold);
    Player player = new Player.PlayerBuilder("Test").build();
    //Act
    goldAction.execute(player);
    //Assert
    assertEquals(gold, player.getGold());
  }

  @Test
  void execute_negative_value_is_subtracted_from_player_gold() throws PlayerMissingException {
    //Arrange
    int gold = -100;
    GoldAction goldAction = new GoldAction(gold);
    Player player = new Player.PlayerBuilder("Test").gold(200).build();
    //Act
    goldAction.execute(player);
    //Assert
    assertEquals(100, player.getGold());
  }

  @Test
  void execute_player_gold_set_to_0_when_negative() throws PlayerMissingException {
    //Arrange
    int gold = -100;
    GoldAction goldAction = new GoldAction(gold);
    Player player = new Player.PlayerBuilder("Test").build();
    //Act
    goldAction.execute(player);
    //Assert
    assertEquals(0, player.getGold());
  }

  @Test
  void execute_zero_value_nothing_is_added_to_player_gold() throws PlayerMissingException {
    //Arrange
    int gold = 0;
    int originalAmountOfGold = 12;
    GoldAction goldAction = new GoldAction(gold);
    Player player = new Player.PlayerBuilder("Test").gold(originalAmountOfGold).build();
    //Act
    goldAction.execute(player);
    //Assert
    assertEquals(originalAmountOfGold, player.getGold());
  }

  @Test
  void execute_player_null_throws_exception(){
    //Arrange
    int gold = 100;
    GoldAction goldAction = new GoldAction(gold);
    Player player = null;
    //Act and Assert
    assertThrows(PlayerMissingException.class, () -> goldAction.execute(player));
  }

}