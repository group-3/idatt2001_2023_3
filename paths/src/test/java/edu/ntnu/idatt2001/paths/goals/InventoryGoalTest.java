package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.exceptions.ItemMissingException;
import edu.ntnu.idatt2001.paths.game.Player;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InventoryGoalTest {

  @Test
  void isFulfilled_only_have_one_of_the_items_in_inventory() throws ItemMissingException {
    //Arrange
    List<String> goalToHaveInInventory = new ArrayList<>();
    goalToHaveInInventory.add("test1");
    goalToHaveInInventory.add("test2");
    InventoryGoal inventoryGoal = new InventoryGoal(goalToHaveInInventory);
    Player player = new Player.PlayerBuilder("Test").build();
    player.addToInventory("test1");
    player.addToInventory("test3");
    //Act
    //Assert
    assertFalse(inventoryGoal.isFulfilled(player));
  }

  @Test
  void isFulfilled_only_have_both_of_the_items_in_inventory() throws ItemMissingException {
    //Arrange
    List<String> goalToHaveInInventory = new ArrayList<>();
    goalToHaveInInventory.add("test1");
    goalToHaveInInventory.add("test2");
    InventoryGoal inventoryGoal = new InventoryGoal(goalToHaveInInventory);
    Player player = new Player.PlayerBuilder("Test").build();
    player.addToInventory("test1");
    player.addToInventory("test2");
    player.addToInventory("test3");
    //Act
    //Assert
    assertTrue(inventoryGoal.isFulfilled(player));
  }

  @Test
  void isFulfilled_non_of_the_items_in_inventory() throws ItemMissingException {
    //Arrange
    List<String> goalToHaveInInventory = new ArrayList<>();
    goalToHaveInInventory.add("test1");
    goalToHaveInInventory.add("test2");
    InventoryGoal inventoryGoal = new InventoryGoal(goalToHaveInInventory);
    Player player = new Player.PlayerBuilder("Test").build();
    player.addToInventory("test3");
    //Act
    //Assert
    assertFalse(inventoryGoal.isFulfilled(player));
  }
}