package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HealthActionTest {

    @Test
    void execute_positive_value_is_added_to_player_health() throws PlayerMissingException {
      //Arrange
      int health = 100;
      int originalAmountOfHealth = 100;
      HealthAction healthAction = new HealthAction(health);
        Player player = new Player.PlayerBuilder("Test").health(originalAmountOfHealth).build();
      //Act
      healthAction.execute(player);
      //Assert
      assertEquals(health + originalAmountOfHealth, player.getHealth());
    }

    @Test
    void execute_negative_value_is_subtracted_from_player_health() throws PlayerMissingException {
      //Arrange
      int health = -10;
      int originalAmountOfHealth = 100;
      HealthAction healthAction = new HealthAction(health);
        Player player = new Player.PlayerBuilder("Test").health(originalAmountOfHealth).build();
      //Act
      healthAction.execute(player);
      //Assert
      assertEquals(originalAmountOfHealth + health, player.getHealth());
    }

    @Test
    void execute_zero_value_nothing_is_added_to_player_health() throws PlayerMissingException {
      //Arrange
      int health = 0;
      int originalAmountOfHealth = 100;
      HealthAction healthAction = new HealthAction(health);
      Player player = new Player.PlayerBuilder("Test").health(originalAmountOfHealth).build();
      //Act
      healthAction.execute(player);
      //Assert
      assertEquals(originalAmountOfHealth, player.getHealth());
    }

    @Test
    void execute_player_null_throws_exception(){
      //Arrange
      int health = 0;
      HealthAction healthAction = new HealthAction(health);
      Player player = null;
      //Act and Assert
      assertThrows(PlayerMissingException.class, () -> healthAction.execute(player));
    }
}