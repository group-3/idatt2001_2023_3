package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScoreActionTest {

    @Test
    void execute_positive_value_is_added_to_player_score() throws PlayerMissingException {
      //Arrange
      int score = 10;
      int originalAmountOfScore = 1;
      ScoreAction scoreAction = new ScoreAction(score);
      Player player = new Player.PlayerBuilder("Test").score(originalAmountOfScore).build();
      //Act
      scoreAction.execute(player);
      //Assert
      assertEquals(score + originalAmountOfScore, player.getScore());
    }

  @Test
  void execute_negative_value_is_added_to_player_score() throws PlayerMissingException {
    //Arrange
    int score = -2;
    int originalAmountOfScore = 3;
    ScoreAction scoreAction = new ScoreAction(score);
    Player player = new Player.PlayerBuilder("Test").score(originalAmountOfScore).build();
    //Act
    scoreAction.execute(player);
    //Assert
    assertEquals(score + originalAmountOfScore, player.getScore());
  }

    @Test
    void execute_player_score_to_negative_value() throws PlayerMissingException {
      //Arrange
      int score = -10;
      int originalAmountOfScore = 1;
      ScoreAction scoreAction = new ScoreAction(score);
      Player player = new Player.PlayerBuilder("Test").score(originalAmountOfScore).build();
      //Act
      scoreAction.execute(player);
      //Assert
      assertEquals(0, player.getScore());
    }

    @Test
    void execute_zero_value_nothing_is_added_to_player_score() throws PlayerMissingException {
      //Arrange
      int score = 0;
      int originalAmountOfScore = 1;
      ScoreAction scoreAction = new ScoreAction(score);
      Player player = new Player.PlayerBuilder("Test").score(originalAmountOfScore).build();
      //Act
      scoreAction.execute(player);
      //Assert
      assertEquals(score + originalAmountOfScore, player.getScore());
    }

    @Test
    void execute_player_null_throws_exception(){
      //Arrange
      int score = 0;
      ScoreAction scoreAction = new ScoreAction(score);
      Player player = null;
      //Act and Assert
      assertThrows(PlayerMissingException.class, () -> scoreAction.execute(player));
    }
}