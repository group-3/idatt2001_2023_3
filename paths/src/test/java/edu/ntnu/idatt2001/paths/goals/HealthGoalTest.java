package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.game.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HealthGoalTest {

    @Test
    void isFulfilled_has_fulfilled_health_goal_over() {
      //Arrange
      int health = 14;
      int healthAmountGoal = 12;
      Player player = new Player.PlayerBuilder("Test").health(health).build();
      HealthGoal healthGoal = new HealthGoal(healthAmountGoal);
      //Act
      //Assert
      assertTrue(healthGoal.isFulfilled(player));
    }

    @Test
    void isFulfilled_has_fulfilled_health_goal_exactly() {
      //Arrange
      int healthAmountGoal = 100;
      Player player = new Player.PlayerBuilder("Test").health(healthAmountGoal).build();
      HealthGoal healthGoal = new HealthGoal(healthAmountGoal);
      //Act
      //Assert
      assertTrue(healthGoal.isFulfilled(player));
    }

    @Test
    void isFulfilled_has_not_fulfilled_health_goal() {
      //Arrange
      int healthAmountGoal = 15;
      Player player = new Player.PlayerBuilder("Test").health(10).build();
      HealthGoal healthGoal = new HealthGoal(healthAmountGoal);
      //Act
      //Assert
      assertFalse(healthGoal.isFulfilled(player));
    }

    @Test
    void isFulfilled_has_negative_minimum_health() throws IllegalArgumentException {
      //Arrange
      int healthAmountGoal = -5;
      //Act
      //Assert
      assertThrows(IllegalArgumentException.class, ()->new GoldGoal(healthAmountGoal));
    }
}