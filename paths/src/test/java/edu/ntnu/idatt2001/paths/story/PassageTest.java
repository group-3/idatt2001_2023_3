package edu.ntnu.idatt2001.paths.story;

import edu.ntnu.idatt2001.paths.exceptions.LinkMissingException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PassageTest {

  @Test
  void addLink_to_link_list() throws LinkMissingException {
    //Arrange
    String reference = "abc123";
    String text = "Test";
    String title = "Passage";
    Passage passage = new Passage(title, text);
    Link link = new Link(text, reference);
    //Act
    passage.addLink(link);
    //assert
    assertTrue(passage.getLinks().contains(link));
  }

  @Test
  void addLink_null_to_list() {
    //Arrange
    String text = "Test";
    String title = "Passage";
    Passage passage = new Passage(title, text);
    Link link = null;
    //Act
    //assert
    assertThrows(LinkMissingException.class, () -> passage.addLink(link));
  }

  @Test
  void hasLinks_with_no_links() {
    //Arrange
    String text = "Test";
    String title = "Passage";
    Passage passage = new Passage(title, text);
    //Act
    passage.hasLinks();
    //Assert
    assertFalse(passage.hasLinks());
  }

  @Test
  void hasLinks_with_links() throws LinkMissingException {
    //Arrange
    String reference = "cde456";
    String text = "Test";
    String title = "Passage";
    Link link = new Link(text,reference);
    Passage passage = new Passage(title, text);
    passage.addLink(link);
    //Act
    passage.hasLinks();
    //Assert
    assertTrue(passage.hasLinks());
  }

  @Test
  void testEquals_with_null() {
  //Arrange
  String text = "Test";
  String title = "Passage";
  Passage passage = new Passage(title,text);
  //Act
  Boolean isEqual = passage.equals(null);
  //Assert
  assertFalse(isEqual);
  }

  @Test
  void testEquals_with_string() {
    //Arrange
    String text = "Test";
    String title = "Passage";
    Passage passage = new Passage(title,text);
    //Act
    Boolean isEqual = passage.equals("Hello");
    //Assert
    assertFalse(isEqual);
  }

  @Test
  void testEquals_with_non_equal_passage() {
    //Arrange
    String text = "Test";
    String title = "Passage";
    Passage passage = new Passage(title,text);
    Passage notEqualPassage = new Passage("NotEqual", text);
    //Act
    Boolean isEqual = passage.equals(notEqualPassage);
    //Assert
    assertFalse(isEqual);
  }

  @Test
  void testEquals_with_two_equal_objects() {
    //Arrange
    String text = "Test";
    String title = "Passage";
    Passage passage = new Passage(title,text);
    //Act
    Boolean isEqual = passage.equals(passage);
    //Assert
    assertTrue(isEqual);
  }

  @Test
  void testEquals_with_equal_passage() {
    //Arrange
    String text = "Test";
    String title = "Passage";
    Passage passage = new Passage(title,text);
    Passage equalPassage = new Passage(title, "NotEqual");
    //Act
    Boolean isEqual = passage.equals(equalPassage);
    //Assert
    assertTrue(isEqual);
  }
}