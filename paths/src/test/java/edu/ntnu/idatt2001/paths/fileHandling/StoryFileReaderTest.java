package edu.ntnu.idatt2001.paths.fileHandling;

import edu.ntnu.idatt2001.paths.exceptions.*;
import edu.ntnu.idatt2001.paths.story.Story;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StoryFileReaderTest {

  @Test
  void ReadFromFile() throws PassageMissingException, LinkMissingException, FileFormatException, ActionMissingException, ReaderProblemException {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);
    Story story = reader.createStoryFromFile(file);

    assertEquals("Haunted House", story.getTitle());
    assertEquals(2, story.getPassages().size());
    assertEquals(1, story.getBrokenLinks().size());
    assertEquals("Beginnings", story.getOpeningPassage().getTitle());
    assertEquals("You are in a small, dimly lit room. There is a door in front of you.", story.getOpeningPassage().getContent());
    assertEquals(1, story.getOpeningPassage().getLinks().size());
    assertEquals("Try to open the door", story.getOpeningPassage().getLinks().get(0).getText());
    assertEquals("Another room", story.getOpeningPassage().getLinks().get(0).getReference());
    assertEquals(1, story.getOpeningPassage().getLinks().get(0).getActions().size());
    assertEquals("{Score}" + "(10)", story.getOpeningPassage().getLinks().get(0).getActions().get(0).toString());
  }

  @Test
  void ReadFromFileNegativeStoryTitle() {
    String text = """

                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeWrongActionFormat() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeActionStartsLikeActionButIsNotAction() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeStoryOpeningpassage() {
    String text = """
                Haunted House
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeStoryContentToOpeningPassage() {
    String text = """
                Haunted House
                ::Beginnings
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeStoryNoLinkToOpeningpassage() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, ()-> reader.createStoryFromFile(file));
  }


  @Test
  void ReadFromFileNegativeStoryLinkStartsLikeALinkButIsNotALink() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door] Another room
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeStoryWithoutSpaceBetweenPassages() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)
                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeStoryNoPassageTiltle() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeStoryContentToPassage() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  /*
  @Test
  void ReadFromFileNegativeStoryNoLinks() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.

                ::The end
                You are in a small, dimly lit room. There is a door in front of you.
                [Try open the door](Another room)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }
   */

  @Test
  void ReadFromFileNegativeStoryNoLinkButSentence() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                Open the book](The book of spells)
                Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeStoryFirstNoLinkButOneLinkUnder() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                You are in a small, dimly lit room. There is a door in front of you.
                Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }

  @Test
  void ReadFromFileNegativeStoryPassageTwoStartsLikeAlinkButIsNotAlink() {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                [Open the book(The book of spells)
                [Go back](Beginnings
                ...""";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);

    assertThrows(FileFormatException.class, () -> reader.createStoryFromFile(file));
  }
}
