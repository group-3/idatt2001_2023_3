package edu.ntnu.idatt2001.paths.game;

import edu.ntnu.idatt2001.paths.exceptions.LinkMissingException;
import edu.ntnu.idatt2001.paths.exceptions.PassageMissingException;
import edu.ntnu.idatt2001.paths.goals.Goal;
import edu.ntnu.idatt2001.paths.story.Link;
import edu.ntnu.idatt2001.paths.story.Passage;
import edu.ntnu.idatt2001.paths.story.Story;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

  @Test
  void begin_the_game() throws PassageMissingException {
    //Arrange
    Player player = new Player.PlayerBuilder("Test").build();
    Passage openingpassage = new Passage("start", "content");
    Story story = new Story("Test1",openingpassage);
    List<Goal> list = new ArrayList<>();
    Game game = new Game(player,story,list);
    //Act
    //Assert
    assertEquals(game.begin(), openingpassage);
  }

  @Test
  void go() throws LinkMissingException, PassageMissingException {
    //Arrange
    Player player = new Player.PlayerBuilder("Test").build();
    Passage openingpassage = new Passage("start", "content");
    Story story = new Story("Test1",openingpassage);
    Link link = new Link(openingpassage.getTitle(), openingpassage.getTitle());
    List<Goal> list = new ArrayList<>();
    Game game = new Game(player,story,list);
    //Act
    //Assert
    assertEquals(openingpassage,game.go(link));
  }

  @Test
  void go_link_is_null() throws PassageMissingException {
    //Arrange
    Player player = new Player.PlayerBuilder("Test").build();
    Passage openingpassage = new Passage("start", "content");
    Story story = new Story("Test1",openingpassage);
    Link link = null;
    List<Goal> list = new ArrayList<>();
    Game game = new Game(player,story,list);
    //Act
    //Assert
    assertThrows(LinkMissingException.class, ()->game.go(link));
  }
}