package edu.ntnu.idatt2001.paths.fileHandling;

import edu.ntnu.idatt2001.paths.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.exceptions.*;
import edu.ntnu.idatt2001.paths.story.Link;
import edu.ntnu.idatt2001.paths.story.Passage;
import edu.ntnu.idatt2001.paths.story.Story;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class StoryFileWriterTest {
  @Test
  void writeToAndReadFromFile(){
    String text = "Hei!!";
    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileWriter writer = new StoryFileWriter();
    StoryFileReader reader = new StoryFileReader();
    writer.writeToFile(file,text);
    reader.readFromFile(file, false);

    assertEquals(text, reader.readFromFile(file, false));
  }

  @Test
  void writeStoryToFile() throws PassageMissingException, ActionMissingException, LinkMissingException {
    String text = """
                Haunted House
                ::Beginnings
                You are in a small, dimly lit room. There is a door in front of you.
                [Try to open the door](Another room)
                {Score}(10)

                ::Another room
                The door opens to another room. You see a desk with a large, dusty book.
                [Open the book](The book of spells)
                [Go back](Beginnings)
                ...""";

    Passage openingPassage = new Passage("Beginnings",
        "You are in a small, dimly lit room. There is a door in front of you.");
    Link openingLink = new Link("Try to open the door", "Another room");
    openingLink.addAction(new ScoreAction(10));
    openingPassage.addLink(openingLink);

    Passage secondPassage = new Passage("Another room",
        "The door opens to another room. You see a desk with a large, dusty book.");
    Link firstLink = new Link("Open the book", "The book of spells");
    Link secondLink = new Link("Go back", "Beginnings");
    secondPassage.addLink(firstLink);
    secondPassage.addLink(secondLink);

    Story story = new Story("Haunted House", openingPassage);
    story.addPassage(secondPassage);

    File file = new File("src/main/resources/stories/TestFile.paths");
    StoryFileReader reader = new StoryFileReader();
    StoryFileWriter writer = new StoryFileWriter();

    writer.writeStoryToFile(file, story);

    assertEquals(reader.readFromFile(file, true), text);

  }
}
