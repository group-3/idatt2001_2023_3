package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.game.Player;
import org.junit.jupiter.api.Test;
import java.lang.IllegalArgumentException;


import static org.junit.jupiter.api.Assertions.*;

class GoldGoalTest {

  @Test
  void isFulfilled_has_fulfilled_gold_goal_over() {
    //Arrange
    int gold = 14;
    int goldAmountGoal = 12;
    Player player = new Player.PlayerBuilder("Test").gold(gold).build();
    GoldGoal goldGoal = new GoldGoal(goldAmountGoal);
    //Act
    //Assert
    assertTrue(goldGoal.isFulfilled(player));
  }

  @Test
  void isFulfilled_has_fulfilled_gold_goal_exactly() {
    //Arrange
    int goldAmountGoal = 12;
    Player player = new Player.PlayerBuilder("Test").gold(goldAmountGoal).build();
    GoldGoal goldGoal = new GoldGoal(goldAmountGoal);
    //Act
    //Assert
    assertTrue(goldGoal.isFulfilled(player));
  }

  @Test
  void isFulfilled_has_not_fulfilled_gold_goal() {
    //Arrange
    int goldAmountGoal = 12;
    Player player = new Player.PlayerBuilder("Test").gold(10).build();
    GoldGoal goldGoal = new GoldGoal(goldAmountGoal);
    //Act
    //Assert
    assertFalse(goldGoal.isFulfilled(player));
  }

    @Test
  void isFulfilled_has_negative_minimum_gold_() throws IllegalArgumentException {
    //Arrange
    int goldAmountGoal = -5;
    //Act
    //Assert
    assertThrows(IllegalArgumentException.class, ()->new GoldGoal(goldAmountGoal));
  }
}