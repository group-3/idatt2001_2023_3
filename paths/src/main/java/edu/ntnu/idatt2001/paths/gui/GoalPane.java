package edu.ntnu.idatt2001.paths.gui;

import edu.ntnu.idatt2001.paths.goals.Goal;
import edu.ntnu.idatt2001.paths.goals.GoalTypes;
import edu.ntnu.idatt2001.paths.goals.GoldGoal;
import edu.ntnu.idatt2001.paths.goals.HealthGoal;
import edu.ntnu.idatt2001.paths.goals.InventoryGoal;
import edu.ntnu.idatt2001.paths.goals.ScoreGoal;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import lombok.Getter;

/**
 * Class that contains the view of Goal.
 *
 * @author Adele og Maria
 */
public class GoalPane extends GridPane {
  private final Button addGoal = new Button("Add Goal");
  @Getter private final ComboBox<String> goalsType = new ComboBox<>();
  private final ListView<String> listView = new ListView<>();
  @Getter private final TextField goalsTextField = new TextField();
  private final Label errorMsg;
  @Getter private final List<Goal> goals = new ArrayList<>();
  @Getter Button goalHelpButton;
  @Getter String goalHelpButtonString = """
        How to play:\s
        
        You are now on the first page. This part of the
        configuration is optional. You can enter goals if you
        want to play the game with goals to achieve. In the 
        first dropdown menu you choose the type of goal you want
        to achieve. The second field you write the value of the 
        goal. Then press the add goal button to add the goal. Then 
        it should pop up in the list underneath.
                                       """;
  private final Design design;

  /**
   * The constructor of the class.
   *
   * @param design the design class with styling
   */
  public GoalPane(Design design) {
    super();
    this.design = design;
    setHgap(10);
    setVgap(10);

    Label goalsTitle = createLabel("Goals", design.getLowerTitleSize());
    Label chooseGoal = createLabel("Choose goal to set: ", design.getNormalTextSize());
    Label writeGoalLabel = createLabel("Write the value of the goal: ", design.getNormalTextSize());
    Label listOfGoalsLabel = createLabel("List of goals added: ", design.getNormalTextSize());
    errorMsg = createLabel("", design.getNormalTextSize());

    goalHelpButton = createButtonWithImage("images/icons/help.png", 10);


    listView.setPrefSize(100, 70);

    fillGoalsChoices(goalsType);

    goalsTitle.setFont(Font.font(design.getTheFont(), design.getLowerTitleSize()));
    errorMsg.setStyle("-fx-text-fill: red;");
    addGoal.setFont(Font.font(design.getTheFont(), design.getNormalTextSize()));
    addGoal.setStyle(("-fx-background-color: ")
        + Design.colourAsStyleString(design.getButtonBackground()));

    eventAddGoal();

    setConstraints(goalsTitle, 0, 0);
    setConstraints(goalHelpButton, 1, 0);

    setConstraints(chooseGoal, 0, 1);
    setConstraints(goalsType, 1, 1);

    setConstraints(writeGoalLabel, 0, 2);
    setConstraints(goalsTextField, 1, 2);

    setConstraints(addGoal, 0, 3);

    setConstraints(errorMsg, 0, 4);

    setConstraints(listOfGoalsLabel, 0, 5);
    setConstraints(listView, 1, 5);

    getChildren().addAll(
        goalsTitle, chooseGoal, goalsType,
        writeGoalLabel, goalsTextField, addGoal,
        listOfGoalsLabel, listView, errorMsg, goalHelpButton
    );
  }

  /**
   * Creates, styles and returns an ImageView.
   *
   * @param imageInfo A string that represents the image reference.
   * @param fitWidth An integer representing the image's width.
   * @return An ImageView created from the parameters.
   */
  private ImageView addIcon(String imageInfo, int fitWidth) {
    Image image = new Image(imageInfo);
    ImageView imageView = new ImageView();
    imageView.setImage(image);
    imageView.setFitWidth(fitWidth);
    imageView.setPreserveRatio(true);
    imageView.setSmooth(true);
    imageView.setCache(true);

    return imageView;
  }

  /**
   * Creates, styles and returns a button with an image on, uses
   * {@link Design#getButtonBackground()}, {@link #styleBackgroundColour(Color)}.
   *
   * @param imageInfo A String representing the image's reference.
   * @param fitWidth An integer representing the image's fit width.
   * @return A styled Button with an image and preferred fit width.
   */
  private Button createButtonWithImage(String imageInfo, int fitWidth) {
    Button newButton = new Button();
    newButton.setGraphic(addIcon(imageInfo, fitWidth));
    newButton.setStyle(styleBackgroundColour(design.getButtonBackground()));
    return newButton;
  }

  /**
   * Takes a Color and uses it to
   * make it compatible with styling a Node's background colour.
   *
   * @param colour A Color representing the colour to be styled.
   * @return A String representation of the background Color.
   */
  private String styleBackgroundColour(Color colour) {
    return ("-fx-background-color: ") + Design.colourAsStyleString(colour);
  }

  /**
   * Method that fills up the list with all types of goals.
   *
   * @param goalsType A comboBox of goalsTypes
   */
  private void fillGoalsChoices(ComboBox<String> goalsType) {
    goalsType.getItems().add(GoalTypes.GOLD);
    goalsType.getItems().add(GoalTypes.HEALTH);
    goalsType.getItems().add(GoalTypes.INVENTORY);
    goalsType.getItems().add(GoalTypes.SCORE);
  }

  /**
   * Method that creates and returns a label.
   *
   * @param labelName The text that the label consist of
   * @param fontSize The size of the font
   * @return a label
   */
  private Label createLabel(String labelName, int fontSize) {
    Label label = new Label(labelName);
    label.setFont(Font.font(design.getTheFont(), fontSize));
    label.setStyle("-fx-text-fill: " + Design.colourAsStyleString(design.getLabelTextColour()));
    return label;
  }

  /**
   * Method that handles click on the add event button.
   */
  private void eventAddGoal() {
    addGoal.setOnAction(event -> {
      errorMsg.setText("");
      if (goalsType.getValue() == null || goalsType.getValue().isEmpty()) {
        errorMsg.setText("Please select valid goal");
        return;
      }
      String goalTypeText = goalsType.getValue();

      String goalText = goalsTextField.getText();

      if (goalText == null || goalText.trim().isEmpty()) {
        errorMsg.setText("Please select valid goal value");
        return;
      }

      String invalidInteger = "Invalid integer";
      String invalidIntegerValue = "Integer must be positive";

      switch (goalTypeText) {
        case GoalTypes.HEALTH -> {
          int goalValue;
          try {
            goalValue = Integer.parseInt(goalText);
          } catch (NumberFormatException e) {
            errorMsg.setText(invalidInteger);
            return;
          }

          if (goalValue < 0) {
            errorMsg.setText(invalidIntegerValue);
            return;
          }

          HealthGoal goal = (HealthGoal) goals.stream()
              .filter(HealthGoal.class::isInstance)
              .findFirst()
              .orElse(null);

          if (goal != null) {
            goals.remove(goal);
          }

          goal = new HealthGoal(goalValue);
          goals.add(goal);

        }
        case GoalTypes.GOLD -> {
          int goalValue;
          try {
            goalValue = Integer.parseInt(goalText);
          } catch (NumberFormatException e) {
            errorMsg.setText(invalidInteger);
            return;
          }

          if (goalValue < 0) {
            errorMsg.setText(invalidIntegerValue);
            return;
          }

          GoldGoal goal = (GoldGoal) goals.stream()
              .filter(GoldGoal.class::isInstance)
              .findFirst()
              .orElse(null);

          if (goal != null) {
            goals.remove(goal);
          }

          goal = new GoldGoal(goalValue);
          goals.add(goal);
        }
        case GoalTypes.SCORE -> {
          int goalValue;
          try {
            goalValue = Integer.parseInt(goalText);
          } catch (NumberFormatException e) {
            errorMsg.setText(invalidInteger);
            return;
          }

          if (goalValue < 0) {
            errorMsg.setText(invalidIntegerValue);
            return;
          }

          ScoreGoal goal = (ScoreGoal) goals.stream()
              .filter(ScoreGoal.class::isInstance)
              .findFirst()
              .orElse(null);

          if (goal != null) {
            goals.remove(goal);
          }

          goal = new ScoreGoal(goalValue);
          goals.add(goal);
        }
        case GoalTypes.INVENTORY -> {

          InventoryGoal goal = (InventoryGoal) goals.stream()
              .filter(InventoryGoal.class::isInstance)
              .findFirst()
              .orElse(null);

          if (goal == null) {
            goal = new InventoryGoal();
            goal.addMandatoryItem(goalText);
            goals.add(goal);
          } else {
            goal.addMandatoryItem(goalText);
          }
        }
        default -> {
          errorMsg.setText("Undefined goal");
          return;
        }
      }
      listView.getItems().clear();
      for (Goal goal : goals) {
        listView.getItems().add(goal.prettyText());
      }
    });
  }
}
