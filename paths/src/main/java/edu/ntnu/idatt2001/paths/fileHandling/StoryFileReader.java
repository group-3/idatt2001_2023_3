package edu.ntnu.idatt2001.paths.fileHandling;

import edu.ntnu.idatt2001.paths.actions.Action;
import edu.ntnu.idatt2001.paths.actions.GoldAction;
import edu.ntnu.idatt2001.paths.actions.HealthAction;
import edu.ntnu.idatt2001.paths.actions.InventoryAction;
import edu.ntnu.idatt2001.paths.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.exceptions.ActionMissingException;
import edu.ntnu.idatt2001.paths.exceptions.FileFormatException;
import edu.ntnu.idatt2001.paths.exceptions.LinkMissingException;
import edu.ntnu.idatt2001.paths.exceptions.PassageMissingException;
import edu.ntnu.idatt2001.paths.exceptions.ReaderProblemException;
import edu.ntnu.idatt2001.paths.story.Link;
import edu.ntnu.idatt2001.paths.story.Passage;
import edu.ntnu.idatt2001.paths.story.Story;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class that reads from file.
 *
 * @author Adele and Maria
 */
public class StoryFileReader {
    /**
     * This method creates a story from a file.
     *
     * @param file the path to the files location
     * @return returns a story
     * @throws FileFormatException throws if the file is in the wrong format
     * @throws ActionMissingException throws if there is no action
     * @throws LinkMissingException throws if there is no link
     * @throws PassageMissingException throws if there is no passage
     *
     */
 public Story createStoryFromFile(File file) throws FileFormatException, ActionMissingException, LinkMissingException, PassageMissingException, ReaderProblemException {
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      String title = reader.readLine();
      if ((title != null) && (!title.isEmpty())) {
        List<List<String>> listOfPassages = readPassages(reader);
        List<Passage> passages = createPassages(listOfPassages);

        if (passages.isEmpty()) {
          throw new FileFormatException("Problem with file: There is no Passages in the file.");
        }

        Story story = new Story(title, passages.get(0));

        for (Passage passage : passages) {
            story.addPassage(passage);
        }

        return story;
      } else {
         throw new FileFormatException("Problem with file: The first line in the file has to be the Story's title.");
      }
    } catch (IOException e) {
       throw new ReaderProblemException("There is a problem with the reader");
    }
 }

    /**
     * This method read the lines of one passage from the file.
     *
     * @param reader reader that reads the lines
     * @return returns a list of a list of strings. Where the String are the lines in a passage
     * @throws IOException if an IO error occurs
     *
     */
 private List<List<String>> readPassages(BufferedReader reader) throws IOException {
    String line = reader.readLine();
    List<List<String>> passages = new ArrayList<>();
    while (line != null && !line.equals("...")) {
      List<String> passageLines = new ArrayList<>();
      while (line != null && !line.isEmpty() && !line.equals("...")) {
        passageLines.add(line);
        line = reader.readLine();
      }
      passages.add(passageLines);

      if (line != null && !line.equals("...")) {
        line = reader.readLine();
      }
    }
    return passages;
 }

    /**
     * This method creates a passage from a list of lists of Strings.
     *
     * @param passages a List of Lists of Strings
     * @return Returns a list of passages
     * @throws FileFormatException throws if the file is in wrong format
     * @throws ActionMissingException throws if there is no actions
     * @throws LinkMissingException throws if there is no link
     *
     */
 private List<Passage> createPassages(List<List<String>> passages) throws FileFormatException, ActionMissingException, LinkMissingException {
    List<Passage> transformedPassages = new ArrayList<>();

   for (List<String> passageLines : passages) {
     transformedPassages.add(createSinglePassage(passageLines));
   }

   return transformedPassages;
 }

    /**
     * This method creates one single passage.
     *
     * @param passageLines a list of Strings containing the lines of a passage
     * @return A passage
     * @throws FileFormatException throws if the file is in wrong format
     * @throws ActionMissingException throws if there is no actions
     * @throws LinkMissingException throws if there is no link
     *
     */
 private Passage createSinglePassage(List<String> passageLines) throws FileFormatException, ActionMissingException, LinkMissingException {
    if (passageLines != null && passageLines.size() >= 2) {
      String lineOne = passageLines.get(0);
      if (!(lineOne != null && lineOne.length() > 2 && lineOne.startsWith("::"))) {
        throw new FileFormatException("Problem with file: The first line in the passage has to be the passages title.");
      }

      String lineTwo = passageLines.get(1);
      if (!(lineTwo != null && !lineTwo.isEmpty() && lineTwo.matches("^[a-zA-Z].*"))) {
        throw new FileFormatException("Problem with file: The second line in the passage has to be the passages content");
      }

      String passageTitle = lineOne.substring(2);
      Passage passage = new Passage(passageTitle, lineTwo);

      List<Link> links = createLinks(passageLines);
      if (!links.isEmpty()) {
        for (Link link : links) {
          passage.addLink(link);
        }
      }
      return passage;

    } else {
      throw new FileFormatException("Invalid passage");
    }
 }

    /**
     * This method creates links.
     *
     * @param passageLines a list of Strings containing the lines of a passage
     * @return A list of links
     * @throws FileFormatException throws if the file is in wrong format
     * @throws ActionMissingException throws if there is no actions
     *
     */
 private List<Link> createLinks(List<String> passageLines) throws FileFormatException, ActionMissingException {
    List<Link> links = new ArrayList<>();
    if (passageLines.size() == 2) {
      return links;
    }
    if (passageLines.size() > 2) {
      List<String> linkLines = passageLines.subList(2, passageLines.size());
      String lineOne = linkLines.get(0);

      Iterator<String> it = linkLines.iterator();

      String line = it.next();
      outer:
      while (line != null) {
        if (isLink(line)) {
          Link link = createLinkFromLine(line);
          if (it.hasNext()) {
            line = it.next();
            while (line != null && line.startsWith("{")) {
              if (isAction(line)) {
                Action actionFromLine = createActionFromLine(line);
                link.addAction(actionFromLine);
              } else {
                throw new FileFormatException("Problem with file: Expected action is not in correct format.");
              }
              if (it.hasNext()) {
                line = it.next();
              } else {
                links.add(link);
                break outer;
              }
            }
            links.add(link);
          } else {
            links.add(link);
            break;
          }

        } else {
          throw new FileFormatException("Problem with file: Expected Link is not in correct format.");
        }
      }
    } else {
      throw new FileFormatException("Problem with file: Expected Link is not in correct format."); 
    }
    return links;
 }


    /**
     * This method checks if a String is a link.
     *
     * @param line a String
     * @return True if it is a link, and false if it's not
     *
     */
  private boolean isLink(String line) {
    return (line != null) && (line.startsWith("[")) && (line.endsWith(")")) && (line.contains("]("));
  }

    /**
     * This method creates an action from a String.
     *
     * @param line a String
     * @return An action
     * @throws FileFormatException throws if the file is in wrong format
     */
  private Action createActionFromLine(String line) throws FileFormatException {
    Action action;
    String[] splitLine = line.split("}");
    String actionType = splitLine[0].replace("{", "").replace("}", "");
    String actionResult = splitLine[1].replace("(", "").replace(")", "");
    switch (actionType) {
      case "Gold" -> {
        try {
          int gold = Integer.parseInt(actionResult);
          action = new GoldAction(gold);
          return action;
        } catch (Exception e) {
          throw new FileFormatException("Gold in GoldAction has to be a number");
        }
      }
      case "Health" -> {
        try {
          int health = Integer.parseInt(actionResult);
          action = new HealthAction(health);
          return action;
        } catch (Exception e) {
          throw new FileFormatException("Health in HealthAction has to be a number");
        }
      }
      case "Inventory" -> {
        action = new InventoryAction(actionResult);
        return action;
      }
      case "Score" -> {
        try {
          int score = Integer.parseInt(actionResult);
          action = new ScoreAction(score);
          return action;
        } catch (Exception e) {
          throw new FileFormatException("Score in ScoreAction has to be a number");
        }
      }
    }
    return null;
  }

    /**
     * This method checks if a String is an action.
     *
     * @param line a String
     * @return True if it is an action and false if it's not
     *
     */
  private boolean isAction(String line) {
    ArrayList<String> actionsAsString = new ArrayList<>();
    actionsAsString.add("Gold");
    actionsAsString.add("Health");
    actionsAsString.add("Inventory");
    actionsAsString.add("Score");
    if ((line != null) && (line.startsWith("{")) && (line.endsWith(")")) && (line.contains("}("))) {
      String actionLine = line.split("}")[0].replace("{", "").replace("}", "");
      return (actionsAsString.contains(actionLine));
    }
    return false;
  }

    /**
     * This method creates a link from a String.
     *
     * @param line a String
     * @return a Link
     *
     */
  private Link createLinkFromLine(String line) {
    String[] splitLine = line.split("]");
    String linkText = splitLine[0].replace("[", "").replace("]", "");
    String linkReference = splitLine[1].replace("(", "").replace(")", "");
    return new Link(linkText, linkReference);

  }

    /**
     * This method reads from file.
     *
     * @param file the path to the files location
     * @return returns a String
     *
     */
  public String readFromFile(File file, boolean isPathsFile) {
    StringBuilder storyText = new StringBuilder();
    try {
      BufferedReader reader = new BufferedReader(new FileReader(file));
      try {
        String line;
        while (((line = reader.readLine()) != null)) {
          if (!(line.equals("...")) && (isPathsFile)) {
            storyText.append(line).append("\n");
          } else {
            storyText.append(line);
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
      try {
        reader.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    return storyText.toString();
  }
}