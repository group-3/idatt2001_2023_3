package edu.ntnu.idatt2001.paths.story;

import edu.ntnu.idatt2001.paths.exceptions.LinkMissingException;
import edu.ntnu.idatt2001.paths.exceptions.PassageMissingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a story with Passages connected by Links.
 *
 * @author Adele and Maria
 */
public class Story {
  private final String title;
  private final Map<Link, Passage> passages;
  private final Passage openingPassage;

  /**
   * Creates a Story with an opening passage and a title.
   *
   * @param title          A string representing the Story's title.
   * @param openingPassage A Passage representing the Story's opening passage.
   * @throws PassageMissingException  If the Story's opening passage is null.
   * @throws IllegalArgumentException If the Story's title is an empty string.
   */
  public Story(String title, Passage openingPassage) throws PassageMissingException {
    if (title == null || title.equals("")) {
      throw new IllegalArgumentException("The Story's title cannot be empty.");
    }
    if (openingPassage == null) {
      throw new PassageMissingException("Opening passage cannot be null.");
    }
    this.title = title;
    this.openingPassage = openingPassage;
    this.passages = new HashMap<>();
    Link link = new Link(openingPassage.getTitle(), openingPassage.getTitle());
    this.passages.put(link, openingPassage);
  }


  /**
   * Adds a new Passage to the Story, creates a Link with the Passage's
   * title as text and reference by using {@link Passage#getTitle()}.
   *
   * @param passage Represents a Passage that gets added to the Story.
   * @throws PassageMissingException If the Passage is null.
   */
  public void addPassage(Passage passage) throws PassageMissingException {
    if (passage == null) {
      throw new PassageMissingException("Passage is null");
    }
    String passageTitle = passage.getTitle();
    Link link = new Link(passageTitle, passageTitle);
    this.passages.put(link, passage);
  }

  /**
   * Removes a passage from the Story based on the Link connected to the Passage. Uses
   * private method {@link #passageIsLinked(Passage)} to check if the Passage
   * is linked to a previous Passage.
   *
   * @param link Represents the Link that is connected to the Passage.
   * @throws IllegalArgumentException If there is a Passage linked to the Passage.
   */
  public void removePassage(Link link) {
    Passage passageRemove = passages.get(link);
    if (this.passageIsLinked(passageRemove)) {
      throw new IllegalArgumentException("Cannot remove a passage that is linked to.");
    }
    passages.remove(link);
  }

  /**
   * Checks if the Passage is linked to a previous Passage.
   *
   * @param passage A Passage that gets checked.
   * @return A boolean representing if the Passage linked to a Passage.
   */
  private boolean passageIsLinked(Passage passage) {
    Link link = new Link(passage.getTitle(), passage.getTitle());
    return passages.values()
        .stream()
        .anyMatch(compareToPassage -> compareToPassage.getLinks().contains(link));
  }

  /**
   * Retrieves a list of broken Links, if the Story has any broken Links. Uses
   * {@link Passage#getLinks()}.
   *
   * @return A List containing the broken Links.
   */
  public List<Link> getBrokenLinks() {
    List<Link> brokenLinks = new ArrayList<>();
    passages.values().forEach(currentPassage ->
        currentPassage.getLinks().stream().filter(link ->
            !(passages.containsKey(link))).forEach(brokenLinks::add));
    return brokenLinks;
  }


  /**
   * Retrieves and returns the Story's title.
   *
   * @return A string representing the Story's title.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Retrieves and returns a Passage based on the parameter Link.
   *
   * @param link Represents the Link connected to the Passage in the Story.
   * @return The Passage that the Link is connected to.
   * @throws LinkMissingException     If the Link is null.
   * @throws IllegalArgumentException If the Link is not connected to a Passage.
   */
  public Passage getPassage(Link link) throws LinkMissingException {
    if (link == null) {
      throw new LinkMissingException("The Link cannot be null.");
    }
    if (!(passages.containsKey(link))) {
      throw new IllegalArgumentException("The Link is not connected to a Passage in the Story.");
    }
    return passages.get(link);
  }

  /**
   * Retrieves and returns an unmodifiable Collection of the Story's Passages.
   *
   * @return A Collection containing the Story's Passages.
   */
  public Collection<Passage> getPassages() {
    return Collections.unmodifiableCollection(passages.values());

  }

  /**
   * Retrieves and returns the Story's opening Passage.
   *
   * @return A Passage representing the Story's opening Passage.
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }

  /**
   * Creates and returns a String representation of a story in the given form.
   *
   * @return a String representation of a story
   */
  @Override
  public String toString() {
    StringBuilder storyAsString = new StringBuilder();
    storyAsString.append(title);
    storyAsString.append(openingPassage.getStringForFile());

    Collection<Passage> passagesCollection = getPassages();
    passagesCollection.stream().filter(passage ->
        !passage.equals(openingPassage))
        .forEach(passage ->
            storyAsString.append("\n").append(passage.getStringForFile()));
    storyAsString.append("\n...");
    return String.valueOf(storyAsString);
  }

}

