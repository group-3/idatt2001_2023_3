package edu.ntnu.idatt2001.paths.gui;

/**
 * Launches the StoryApp.
 *
 * @author Adele and Maria
 */
public class StoryAppLauncher {

  /**
   * Main method for the launcher.
   *
   * @param args Arguments from console.
   */
  public static void main(String[] args) {
    StoryApp.main(args);
  }
}
