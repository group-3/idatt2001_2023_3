package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;

/**
 * HealthAction implements the Action interface and is
 * an action that changes the player's health.
 *
 * @author Adele and Maria
 */
public class HealthAction implements Action {
  private final int health;

  /**
   * Creates a health action that changes the player's health.
   *
   * @param health An integer representing
   */
  public HealthAction(int health) {
    this.health = health;
  }

  /**
   * Executes the HealthAction using {@link #changeHealth(boolean, Player)}.
   *
   * @param player Represents a player.
   * @throws PlayerMissingException If the Player is null.
   */
  public void execute(Player player) throws PlayerMissingException {
    if (player == null) {
      throw new PlayerMissingException("Player is missing");
    }
    changeHealth(false, player);
  }

  /**
   * Reverts the HealthAction using {@link #changeHealth(boolean, Player)}.
   *
   * @param player Represents a Player.
   * @throws PlayerMissingException If the Player is null.
   */
  @Override
  public void revert(Player player) throws PlayerMissingException {
    if (player == null) {
      throw new PlayerMissingException("Player is null");
    }
    changeHealth(true, player);
  }

  /**
   * Changes the player's health by using
   * {@link Player#getHealth()} and {@link Player#addHealth(int)},
   * if the player's health becomes negative then {@link Player#setHealth(int)} is
   * used to set the player's health to 0.
   *
   * @param isRevert A boolean representing if the score is being reverted or not.
   * @param player Represents a Player.
   */
  private void changeHealth(boolean isRevert, Player player) {
    int healthToChange;

    if (isRevert) {
      healthToChange = -health;
    } else {
      healthToChange = health;
    }

    if ((player.getHealth() + healthToChange) < 0) {
      player.setHealth(0);
    } else {
      player.addHealth(healthToChange);
    }
  }

  /**
   * Returns the Action as a String in a file-friendly format.
   *
   * @return A String representation of the Action.
   */
  @Override
  public String toString() {
    return ("{Health}" + "(" + health + ")");
  }
}
