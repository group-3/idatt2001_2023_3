package edu.ntnu.idatt2001.paths.game;

import java.util.List;
import edu.ntnu.idatt2001.paths.exceptions.LinkMissingException;
import edu.ntnu.idatt2001.paths.story.Link;
import edu.ntnu.idatt2001.paths.story.Passage;
import edu.ntnu.idatt2001.paths.story.Story;
import edu.ntnu.idatt2001.paths.goals.Goal;

/**
 * Class that represents a game.
 *
 * @author Adele and Maria
 */
public class Game {

  private final Player player;
  private final Story story;
  private final List<Goal> goals;

  /**
   * Constructor for a new game with a player, story and goals.
   *
   * @param player the player of the game with name, health, gold and score
   * @param story a story for the game with title and openingPassage
   * @param goals a list of goals in the game
   *
   */
  public Game(Player player, Story story, List<Goal> goals) {
    this.player = player;
    this.story = story;
    this.goals = goals;
  }

  /**
   * This method gets the player of the game.
   *
   * @return the player of the game
   *
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * This method gets the story.
   *
   * @return the story as Story
   *
   */
  public Story getStory() {
    return story;
  }

  /**
   * This method gets the goals of the game.
   *
   * @return a list of goals in the game
   *
   */
  public List<Goal> getGoals() {
    return goals;
  }

  /**
   * This method is the beginning of the game.
   *
   * @return the opening passage as a Passage
   *
   */
  public Passage begin() {
    return story.getOpeningPassage();
  }

  /**
   * This method finds the passage that's connected to the given link.
   *
   * @param link a link that binds the passages
   *
   * @return the passage that's connected to the given link as a Passage
   *
   */
  public Passage go(Link link) throws LinkMissingException {
    if (link == null) {
      throw new LinkMissingException("A link cant be null");
    }
    return story.getPassage(link);
  }


}
