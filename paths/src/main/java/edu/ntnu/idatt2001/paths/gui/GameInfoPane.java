package edu.ntnu.idatt2001.paths.gui;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import lombok.Getter;

/**
 * Class that contains the view of GameInfo.
 *
 * @author Adele og Maria
 */
public class GameInfoPane extends GridPane {
  @Getter private final Label storyNameLabel;
  @Getter private final Label storyName;
  @Getter private final Label path;
  @Getter private final Label storyPathlabel;
  @Getter private final Label deadLinks;
  @Getter private final Label deadLinksLabel;
  private final Design design;

  /**
   * Constructor for the class.
   *
   * @param design the design class with styling
   *
   */
  public GameInfoPane(Design design) {
    super();
    this.design = design;

    this.storyNameLabel = createLabel("", design.getSmallTextSize());
    this.storyName = createLabel("", design.getSmallTextSize());
    this.path = createLabel("", design.getSmallTextSize());
    this.storyPathlabel = createLabel("", design.getSmallTextSize());
    this.deadLinks = createLabel("", design.getSmallTextSize());
    this.deadLinksLabel = createLabel("", design.getSmallTextSize());

    setHgap(2);
    setVgap(2);
    setPadding(new Insets(10, 20, 10, 20));

    setConstraints(storyNameLabel, 0, 0);
    setConstraints(storyName, 1, 0);

    setConstraints(storyPathlabel, 0, 1);
    setConstraints(path, 1, 1);

    setConstraints(deadLinksLabel, 0, 2);
    setConstraints(deadLinks, 1, 2);

    getChildren().addAll(
        storyNameLabel, storyPathlabel, deadLinksLabel,
        storyName, path, deadLinks
    );
  }

  /**
   * Method that creates and returns a label.
   *
   * @param labelName The text that the label consist of
   * @param fontSize The size of the font
   * @return a label
   */
  private Label createLabel(String labelName, int fontSize) {
    Label label = new Label(labelName);
    label.setFont(Font.font(design.getTheFont(), fontSize));
    label.setStyle("-fx-text-fill: " + Design.colourAsStyleString(design.getLabelTextColour()));
    return label;
  }
}
