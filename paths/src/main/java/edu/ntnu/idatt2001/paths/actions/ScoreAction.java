package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;

/**
 * ScoreAction implements the Action interface and is an
 * action that changes the player's score.
 *
 * @author Adele and Maria
 */
public class ScoreAction implements Action {
  private final int points;

  /**
   * Creates a score action that changes the Player's score.
   *
   * @param points An integer representing the amount of points.
   */
  public ScoreAction(int points) {
    this.points = points;
  }

  /**
   * Executes the action using {@link #changeScore(boolean, Player)}.
   *
   * @param player Represents a Player.
   * @throws PlayerMissingException If the Player parameter is null.
   */
  @Override
  public void execute(Player player) throws PlayerMissingException {
    if (player == null) {
      throw new PlayerMissingException("Player is missing");
    }
    changeScore(false, player);
  }

  /**
   * Reverts the ScoreAction using {@link #changeScore(boolean, Player)}.
   *
   * @param player Represents a Player.
   * @throws PlayerMissingException If the Player is null.
   */
  @Override
  public void revert(Player player) throws PlayerMissingException {
    if (player == null) {
      throw new PlayerMissingException("Player is null");
    }
    changeScore(true, player);
  }

  /**
   * Changes the Player's score by using {@link Player#addScore(int)} with
   * Action's point amount. Uses {@link Player#getScore()} to check if the Player's
   * new score is less than 0 and uses {@link Player#setScore(int)} to 0.
   *
   * @param isRevert A boolean that represents if the method is reverting the score or not.
   * @param player Represents a Player.
   */
  private void changeScore(boolean isRevert, Player player) {
    int pointsToChange;

    if (isRevert) {
      pointsToChange = -points;
    } else {
      pointsToChange = points;
    }

    if ((player.getScore() + pointsToChange) < 0) {
      player.setScore(0);
    } else {
      player.addScore(pointsToChange);
    }

  }

  /**
   * Returns the Action as a String in a file-friendly format.
   *
   * @return A String representation of the Action.
   */
  @Override
  public String toString() {
    return ("{Score}" + "(" + points + ")");
  }
}
