package edu.ntnu.idatt2001.paths.story;

import edu.ntnu.idatt2001.paths.exceptions.LinkMissingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Represents a Passage connected by Links in a Story.
 *
 * @author Adele and Maria
 */
public class Passage {
  private final String title;
  private final String content;
  private final List<Link> links;

  /**
   * Creates a Passage with a title and content.
   *
   * @param title A string representing the Passage's title.
   * @param content A string representing the Passage's content.
   * @throws IllegalArgumentException If the title or content is an empty string.
   */
  public Passage(String title, String content) {
    if (title == null || title.equals("")) {
      throw new IllegalArgumentException("The Passage's title cannot be empty");
    }
    if (content == null || content.equals("")) {
      throw new IllegalArgumentException("The Passage's content cannot be empty");
    }
    this.title = title;
    this.content = content;
    this.links = new ArrayList<>();
  }

  /**
   * Adds a Link to the passage if the Link exists.
   *
   * @param link Represents a Link that gets added to the Passage.
   * @throws LinkMissingException If the Link is null.
   */
  public void addLink(Link link) throws LinkMissingException {
    if (link == null) {
      throw new LinkMissingException("Link cant be null");
    }
    this.links.add(link);
  }

  /**
   * Checks if the Passage has any Links.
   *
   * @return A boolean representing if the Passage has Links.
   */
  public boolean hasLinks() {
    return !(links.isEmpty());
  }

  /**
   * Retrieves and returns the Passage's Links.
   *
   * @return A list containing the Passage's Links or null if the Passage doesn't
   * have any Links.
   */
  public List<Link> getLinks() {
    return links;
  }

  /**
   * Retrieves and returns the Passage's title.
   *
   * @return A string representing the Passage's title.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Retrieves and returns the Passage's content.
   *
   * @return A string representing the Passage's content.
   */
  public String getContent() {
    return content;
  }

  /**
   * Creates and returns a string representation of the Passage that's compatible
   * with file writing.
   *
   * @return A string representation of the Passage.
   */
  public String getStringForFile() {
    StringBuilder passageAsString = new StringBuilder();
    passageAsString.append("\n::").append(this.title);
    passageAsString.append("\n").append(this.content);
    this.links.forEach(link -> {
      passageAsString.append("\n[")
          .append(link.getText())
          .append("]").append("(")
          .append(link.getReference())
          .append(")");
      link.getActions().forEach(action -> passageAsString.append("\n").append(action.toString()));
    });
    return String.valueOf(passageAsString);
  }

  /**
   * Creates and returns a string representation of the Passage.
   *
   * @return A string representation of the Passage.
   */
  @Override
  public String toString() {
    return links.stream().map(link -> link.toString() + "\n")
        .collect(Collectors.joining("", "Title of passage: "
            + this.title + "\nPassage content: " + this.content
            + "\nLinks in passage:\n ", ""));
  }

  /**
   * Hashes the Passage's title to an integer.
   *
   * @return An integer representing the Passage's hashcode.
   */
  @Override
  public int hashCode() {
    return Objects.hash(title);
  }

  /**
   * Compares this Passage with another Passage, uses {@link #getTitle()}
   * to compare the Passages' titles.
   *
   * @param object A Passage that the current Passage gets compared with.
   * @return A boolean representing if the Passages are equal.
   */
  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof Passage passage)) {
      return false;
    }
    return this.getTitle().equals(passage.getTitle());
  }

}
