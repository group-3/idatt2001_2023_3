package edu.ntnu.idatt2001.paths.fileHandling;

import java.io.File;

public class WriteToFileApp {
  public static void main(String[] args) {
    StoryFileWriter storyFileWriter = new StoryFileWriter();
    String storyText = """
        Haunted House
        ::Beginnings
        You are in a small, dimly lit room. There is a door in front of you.
        [Try to open the door](Another room)

        ::Another room
        The door opens to another room. You see a desk with a large, dusty book.
        [Open the book](The book of spells)
        [Go back](Beginnings)
        ...""";
    File file = new File("src//main//resources//stories//TestFile2.paths");
    storyFileWriter.writeToFile(file, storyText);
  }
}
