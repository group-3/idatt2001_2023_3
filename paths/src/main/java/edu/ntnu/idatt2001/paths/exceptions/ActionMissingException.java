package edu.ntnu.idatt2001.paths.exceptions;

public class ActionMissingException extends Exception {
  public ActionMissingException(String message) {
    super(message);
  }
}
