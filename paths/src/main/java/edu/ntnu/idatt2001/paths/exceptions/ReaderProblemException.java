package edu.ntnu.idatt2001.paths.exceptions;

public class ReaderProblemException extends Exception {
    public ReaderProblemException(String message) {
        super(message);
    }
}
