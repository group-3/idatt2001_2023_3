package edu.ntnu.idatt2001.paths.exceptions;

public class ItemMissingException extends Exception {
  public ItemMissingException(String message) {
    super(message);
  }
}
