package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.game.Player;

/**
 * HealthGoal implements the Goal interface and represents a health
 * goal that is achieved by reaching a minimum amount of health.
 *
 * @author Adele and Maria
 */
public class HealthGoal implements Goal {
  private final int minimumHealth;

  /**
   * Creates a health goal with a minimum amount of health to reach as a parameter.
   *
   * @param minimumHealth An integer representing a minimum health the player has to have to
   *                      achieve the health goal.
   * @throws IllegalArgumentException If the minimum health is negative.
   */
  public HealthGoal(int minimumHealth) {
    if (minimumHealth < 0) {
      throw new IllegalArgumentException("Minimum health for goal cannot be negative.");
    }
    this.minimumHealth = minimumHealth;
  }

  /**
   * Checks if the health goal for a player has been achieved by using {@link Player#getHealth()}
   * to compare the player's health with the minimum health goal.
   *
   * @param player Represents a Player.
   * @return A boolean representing if the health goal was achieved.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return (player.getHealth() >= minimumHealth);
  }

  /**
   * Creates and returns a string representation of the Goal.
   *
   * @return A string representation of the Goal.
   */
  @Override
  public String prettyText() {
    return "Health: " + minimumHealth;
  }
}
