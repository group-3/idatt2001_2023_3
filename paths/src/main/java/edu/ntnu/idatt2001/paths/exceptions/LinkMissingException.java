package edu.ntnu.idatt2001.paths.exceptions;

public class LinkMissingException extends Exception {
  public LinkMissingException(String message) {
    super(message);
  }
}
