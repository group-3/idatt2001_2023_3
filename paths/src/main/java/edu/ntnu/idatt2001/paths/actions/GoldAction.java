package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;

/**
 * GoldAction implements the Action interface and is
 * an action that changes the player's gold.
 *
 * @author Adele and Maria
 */
public class GoldAction implements Action {
  private final int gold;

  /**
   * Creates a gold action that changes the Player's gold.
   *
   * @param gold An integer representing the amount of gold.
   */
  public GoldAction(int gold) {
    this.gold = gold;
  }

  /**
   * Executes the GoldAction using {@link #changeGold(boolean, Player)}.
   *
   * @param player Represents a Player.
   * @throws PlayerMissingException If the Player is null.
   */
  @Override
  public void execute(Player player) throws PlayerMissingException {
    if (player == null) {
      throw new PlayerMissingException("Player is null");
    }
    changeGold(false, player);
  }

  /**
   * Reverts the GoldAction using {@link #changeGold(boolean, Player)}.
   *
   * @param player Represents a Player.
   * @throws PlayerMissingException If the player is null.
   */
  @Override
  public void revert(Player player) throws PlayerMissingException {
    if (player == null) {
      throw new PlayerMissingException("Player is null");
    }
    changeGold(true, player);
  }

  /**
   * Changes the Player's gold amount by using
   * {@link Player#getGold()} and {@link Player#addGold(int)},
   * if the Player's gold amount becomes negative then {@link Player#setGold(int)}
   * is used to change the Player's gold amount to 0 (no debt in this game!).
   *
   * @param isRevert A boolean representing if the method is reverting the score.
   * @param player Represents a Player.
   */
  private void changeGold(boolean isRevert, Player player) {
    int goldToChange;

    if (isRevert) {
      goldToChange = -gold;
    } else {
      goldToChange = gold;
    }
    if ((player.getGold() + goldToChange) < 0) {
      player.setGold(0);
    } else {
      player.addGold(goldToChange);
    }

  }

  /**
   * Returns the Action as a String in a file-friendly format.
   *
   * @return A String representation of the Action.
   */
  @Override
  public String toString() {
    return ("{Gold}" + "(" + gold + ")");
  }
}
