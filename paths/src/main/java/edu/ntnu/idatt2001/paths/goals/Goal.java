package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.game.Player;

/**
 * Interface Goal that represents goals that players
 * have in a game.
 *
 * @author Adele and Maria
 */
public interface Goal {
  /**
   * Checks if the goal has been achieved.
   *
   * @param player Represents a Player.
   * @return A boolean representing if the Goal was achieved.
   */
  boolean isFulfilled(Player player);

  /**
   * A string with a formation of text.
   *
   * @return a string
   */
  String prettyText();
}