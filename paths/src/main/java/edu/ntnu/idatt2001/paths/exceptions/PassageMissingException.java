package edu.ntnu.idatt2001.paths.exceptions;

public class PassageMissingException extends Exception {
  public PassageMissingException(String message) {
    super(message);
  }
}
