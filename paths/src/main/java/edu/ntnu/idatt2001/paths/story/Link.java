package edu.ntnu.idatt2001.paths.story;

import edu.ntnu.idatt2001.paths.actions.Action;
import edu.ntnu.idatt2001.paths.exceptions.ActionMissingException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a link that connects Passages in a Story.
 *
 * @author Adele and Maria
 */
public class Link {
  private final String text;
  private final String reference;
  private final List<Action> actions;

  /**
   * Creates a Link with parameters text and reference.
   *
   * @param text A string representing the Link's text.
   * @param reference A string representing the Link's reference.
   * @throws IllegalArgumentException If the Link's text or reference is empty.
   */
  public Link(String text, String reference) {
    if (text == null || text.equals("")) {
      throw new IllegalArgumentException("The Link's text cannot be empty.");
    }
    if (reference == null || reference.equals("")) {
      throw new IllegalArgumentException("The Link's reference cannot be empty.");
    }
    this.text = text;
    this.reference = reference;
    this.actions = new ArrayList<>();
  }

  /**
   * Returns the Link's text.
   *
   * @return A string representing the Link's text.
   */
  public String getText() {
    return text;
  }

  /**
   * Returns the Link's reference.
   *
   * @return A string representing the Link's reference.
   */
  public String getReference() {
    return reference;
  }

  /**
   * Returns the Link's actions as a List.
   *
   * @return An ArrayList containing the Link's actions.
   */
  public List<Action> getActions() {
    return actions;
  }

  /**
   * Adds an action to the Link.
   *
   * @param action An Action representing an action in the Story.
   * @throws ActionMissingException If the action is missing/null.
   */
  public void addAction(Action action) throws ActionMissingException {
    if (action == null) {
      throw new ActionMissingException("Action cant be null");
    }
    actions.add(action);
  }

  /**
   * Compares this Link with another Link, uses {@link #getReference()} to
   * compare the Links' references.
   *
   * @param object A Link that the current Link gets compared with.
   * @return A boolean representing if the Links are equal.
   */
  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof Link link)) {
      return false;
    }
    return this.getReference().equals(link.getReference());
  }

  /**
   * Hashes the Link's reference and text to an integer.
   *
   * @return An integer representing the Link's hashcode.
   */
  @Override
  public int hashCode() {
    return Objects.hash(reference);
  }

  /**
   * Creates and returns a string representation of the Link.
   *
   * @return A string representing the Link.
   */
  @Override
  public String toString() {
    return "\nText: " + getText()
            + "\nReference: " + getReference()
            + "\nActions: ";
  }

}
