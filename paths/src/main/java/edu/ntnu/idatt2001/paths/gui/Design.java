package edu.ntnu.idatt2001.paths.gui;

import javafx.scene.paint.Color;

/**
 * Represents the design for the Paths application.
 *
 * @author Adele and Maria.
 */
public class Design {
  private final String theFont;
  private final int mainTitleSize;
  private final int lowerTitleSize;
  private final int normalTextSize;
  private final int tooltipSize;
  private final int smallTextSize;
  private final Color sceneBackground;
  private final Color textBackground;
  private final Color buttonBackground;
  private final Color tooltipBackground;
  private final Color labelTextColour;


  /**
   * Constructor for the design.
   */
  public Design() {
    this.theFont = "Arial";

    this.mainTitleSize = 40;
    this.lowerTitleSize = 20;
    this.normalTextSize = 14;
    this.tooltipSize = 12;
    this.smallTextSize = 10;

    this.textBackground = Color.WHITE;
    this.buttonBackground = Color.rgb(234, 216, 193);
    this.sceneBackground = Color.rgb(121, 86, 66);
    this.tooltipBackground = Color.rgb(217, 211, 189);
    this.labelTextColour = Color.rgb(234, 216, 193);
  }

  /**
   * Retrieves and returns the colour of Label's text.
   *
   * @return A Color representing the colour Label's text.
   */
  public Color getLabelTextColour() {
    return labelTextColour;
  }

  /**
   * Retrieves and returns the font size in the design of Tooltips.
   *
   * @return An integer representing the font size in Tooltips.
   */
  public int getTooltipSize() {
    return tooltipSize;
  }

  /**
   * Retrieves and returns the background colour of the Tooltip design.
   *
   * @return A Color representing the background colour in Tooltip.
   */
  public Color getTooltipBackground() {
    return tooltipBackground;
  }

  /**
   * Retrieves and returns the font size of titles in the design.
   *
   * @return An integer representing the font size of titles.
   */
  public int getMainTitleSize() {
    return mainTitleSize;
  }

  /**
   * Retrieves and returns the font size of lower titles in the design.
   *
   * @return An integer representing the font size of lower  titles.
   */
  public int getLowerTitleSize() {
    return lowerTitleSize;
  }

  /**
   * Retrieves and returns the font size of small text in the design.
   *
   * @return An integer representing the font size of lower  titles.
   */
  public int getSmallTextSize() {
    return smallTextSize;
  }

  /**
   * Retrieves and returns the font size of normal text in the design.
   *
   * @return An integer representing the normal text's font size.
   */
  public int getNormalTextSize() {
    return normalTextSize;
  }

  /**
   * Retrieves and returns the background colour in Scenes.
   *
   * @return A Color representing the Scene's background colour.
   */
  public Color getSceneBackground() {
    return sceneBackground;
  }

  /**
   * Retrieves and returns the background colour of black text.
   *
   * @return A Colour representing the background colour.
   */
  public Color getTextBackground() {
    return textBackground;
  }

  /**
   * Retrieves and returns background colour of Buttons.
   *
   * @return A Color representing the background colour.
   */
  public Color getButtonBackground() {
    return buttonBackground;
  }

  /**
   * Retrieves and returns the font used in the design.
   *
   * @return A String representing the font.
   */
  public String getTheFont() {
    return theFont;
  }

  /**
   * Takes a Color and makes it to work with the setStyle-method.
   *
   * @param colour A Color representing the colour to be styled.
   * @return A String representation of the Color.
   */
  public static String colourAsStyleString(Color colour) {
    return (colour.toString().replace("0x", "#"));
  }
}
