package edu.ntnu.idatt2001.paths.exceptions;

public class PlayerMissingException extends Exception {
  public PlayerMissingException(String message) {
    super(message);
  }
}
