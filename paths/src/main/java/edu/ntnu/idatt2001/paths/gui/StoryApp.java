package edu.ntnu.idatt2001.paths.gui;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Application that runs the paths program.
 *
 * @author Adele and Maria
 */
public class StoryApp extends Application {

  /**
   * Start method for the application that sets the home scene.
   *
   * @param stage the primary stage for the application, onto which
   * the application scene can be set.
   */
  @Override
  public void start(Stage stage) {
    Scene homeScene = new HomeController(stage).getHomeScene();
    stage.setScene(homeScene);
  }

  /**
   * Main method for the application.
   *
   * @param args Arguments from console.
   */
  public static void main(String[] args) {
    launch(args);
  }
}
