package edu.ntnu.idatt2001.paths.gui;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import lombok.Getter;

/**
 * Class that contains the view of Game.
 *
 * @author Adele og Maria
 */
public class GamePane extends GridPane {
  @Getter private final Button fileButton = new Button("...");
  @Getter private final ComboBox<String> preDefinedStories = new ComboBox<>();
  @Getter private final Label errorMsg;
  @Getter Button gameHelpButton;
  @Getter String gameHelpButtonString = """
        How to play:\s
        
        You are now on the first page. Under game you
        choose with game you want to play. There is two options.
        Choose one of the pre made games that is located in the last
        field (dropdown men). Er choose one from you own computer.
        
        If you choose your own file, please note that it must 
        have the correct formatting and be a '.paths' file.
        The correct format is;
        
        First line has to be the story's title. Then the next line
        should start with '::' and be the passage title. The line after 
        this is a line containing text and is the passage content.
        Then the next line is links. A link should be in the format
        '[link text](reference to another passage)' after links it is 
        optional to have actions or not. Actions should be in the format
        '{Action type}(Action value)'. There can also be several links. 
        Remember there always has to be a empty line between each passage.  
        
        Good luck on your adventure!
                                       """;
  private final Design design;

  /**
   * The constructor of the class.
   *
   */
  public GamePane(Design design) {
    super();
    setHgap(10);
    setVgap(10);

    this.design = design;

    Label gameTitle = createLabel("Game", design.getLowerTitleSize());
    Label fileLabel = createLabel("Upload game file here: ", design.getNormalTextSize());
    Label preDefinedStoriesLabel = createLabel("Choose existing story: ", design.getNormalTextSize());
    Label orLabel = createLabel("or", design.getNormalTextSize());
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open '.paths' File");

    gameHelpButton = createButtonWithImage("images/icons/help.png", 10);

    fileButton.setFont(Font.font(design.getTheFont(), design.getNormalTextSize()));
    fileButton.setFont(Font.font(design.getTheFont(), design.getNormalTextSize()));
    fileButton.setStyle(("-fx-background-color: ")
        + Design.colourAsStyleString(design.getButtonBackground()));
    fileButton.setMaxWidth(125.0);

    errorMsg = createLabel("", design.getNormalTextSize());
    errorMsg.setStyle("-fx-text-fill: red;");
    fillPreDefinedStories();

    setConstraints(gameTitle, 0, 0);
    setConstraints(gameHelpButton, 1, 0);

    setConstraints(fileLabel, 0, 1);
    setConstraints(fileButton, 1, 1);

    setConstraints(orLabel, 0, 2);

    setConstraints(preDefinedStoriesLabel, 0, 3);
    setConstraints(preDefinedStories, 1, 3);

    setConstraints(errorMsg, 0, 4);

    getChildren().addAll(
        gameTitle, fileButton, fileLabel,
        preDefinedStoriesLabel, preDefinedStories, errorMsg, orLabel, gameHelpButton
    );
  }

  /**
   * Creates, styles and returns an ImageView.
   *
   * @param imageInfo A string that represents the image reference.
   * @param fitWidth An integer representing the image's width.
   * @return An ImageView created from the parameters.
   */
  private ImageView addIcon(String imageInfo, int fitWidth) {
    Image image = new Image(imageInfo);
    ImageView imageView = new ImageView();
    imageView.setImage(image);
    imageView.setFitWidth(fitWidth);
    imageView.setPreserveRatio(true);
    imageView.setSmooth(true);
    imageView.setCache(true);

    return imageView;
  }

  /**
   * Takes a Color and uses it to
   * make it compatible with styling a Node's background colour.
   *
   * @param colour A Color representing the colour to be styled.
   * @return A String representation of the background Color.
   */
  private String styleBackgroundColour(Color colour) {
    return ("-fx-background-color: ") + Design.colourAsStyleString(colour);
  }

  /**
   * Creates, styles and returns a button with an image on, uses
   * {@link Design#getButtonBackground()}, {@link #styleBackgroundColour(Color)}.
   *
   * @param imageInfo A String representing the image's reference.
   * @param fitWidth An integer representing the image's fit width.
   * @return A styled Button with an image and preferred fit width.
   */
  private Button createButtonWithImage(String imageInfo, int fitWidth) {
    Button newButton = new Button();
    newButton.setGraphic(addIcon(imageInfo, fitWidth));
    newButton.setStyle(styleBackgroundColour(design.getButtonBackground()));
    return newButton;
  }

  /**
   * Method that fills up combobox with pre defined stories.
   *
   */
  private void fillPreDefinedStories() {
    preDefinedStories.getItems().add("HauntedHouse");
  }

  /**
   * Method that creates and returns a label.
   *
   * @param labelName The text that the label consist of
   * @param fontSize The size of the font
   * @return a label
   */
  private Label createLabel(String labelName, int fontSize) {
    Label label = new Label(labelName);
    label.setFont(Font.font(design.getTheFont(), fontSize));
    label.setStyle("-fx-text-fill: " + Design.colourAsStyleString(design.getLabelTextColour()));
    return label;
  }
}
