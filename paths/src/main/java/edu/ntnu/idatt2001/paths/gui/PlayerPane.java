package edu.ntnu.idatt2001.paths.gui;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import lombok.Getter;

/**
 * Class that contains the view of Player.
 *
 * @author Adele og Maria
 */
public class PlayerPane extends GridPane {
  @Getter
  private final TextField playerNameField;
  @Getter
  private final TextField playerHealthField;
  @Getter
  private final TextField playerGoldField;
  @Getter Button playerHelpButton;
  @Getter String playerHelpButtonString = """
        How to play:\s
        
        You are now on the first page. Before you start you have to enter
        some information about the player. The player needs a name,
        health and amount of gold to start with. Both health and gold
        has to be a positive number, but health also cant be zero. You
        cant start a game with zero lives left;)
                                       """;
  private final Design design;

  /**
   * Constructor of the method.
   *
   * @param design the design class with styling
   */
  public PlayerPane(Design design) {
    super();
    setHgap(10);
    setVgap(10);

    this.design = design;

    Label playerTitle = createLabel("Player", design.getLowerTitleSize());
    playerHelpButton = createButtonWithImage("images/icons/help.png", 10);

    playerNameField = createTextField();
    Label playerNameLabel = createLabel("Player name:", design.getNormalTextSize());

    playerHealthField = createTextField();
    Label playerHealthLabel = createLabel("Start health:", design.getNormalTextSize());

    playerGoldField = createTextField();
    Label playerGoldLabel = createLabel("Starting gold: ", design.getNormalTextSize());

    setConstraints(playerTitle, 0, 0);
    setConstraints(playerHelpButton, 1, 0);

    setConstraints(playerNameLabel, 0,  1);
    setConstraints(playerNameField, 1, 1);

    setConstraints(playerHealthLabel, 0, 2);
    setConstraints(playerHealthField, 1, 2);

    setConstraints(playerGoldLabel, 0, 3);
    setConstraints(playerGoldField, 1, 3);

    getChildren().addAll(
        playerTitle, playerNameLabel, playerNameField,
        playerHealthLabel, playerHealthField, playerGoldLabel, playerGoldField, playerHelpButton
    );
  }

  /**
   * Creates, styles and returns a button with an image on, uses
   * {@link Design#getButtonBackground()}, {@link #styleBackgroundColour(Color)}.
   *
   * @param imageInfo A String representing the image's reference.
   * @param fitWidth An integer representing the image's fit width.
   * @return A styled Button with an image and preferred fit width.
   */
  private Button createButtonWithImage(String imageInfo, int fitWidth) {
    Button newButton = new Button();
    newButton.setGraphic(addIcon(imageInfo, fitWidth));
    newButton.setStyle(styleBackgroundColour(design.getButtonBackground()));
    return newButton;
  }

  /**
   * Takes a Color and uses it to
   * make it compatible with styling a Node's background colour.
   *
   * @param colour A Color representing the colour to be styled.
   * @return A String representation of the background Color.
   */
  private String styleBackgroundColour(Color colour) {
    return ("-fx-background-color: ") + Design.colourAsStyleString(colour);
  }

  /**
   * Creates, styles and returns an ImageView.
   *
   * @param imageInfo A string that represents the image reference.
   * @param fitWidth An integer representing the image's width.
   * @return An ImageView created from the parameters.
   */
  private ImageView addIcon(String imageInfo, int fitWidth) {
    Image image = new Image(imageInfo);
    ImageView imageView = new ImageView();
    imageView.setImage(image);
    imageView.setFitWidth(fitWidth);
    imageView.setPreserveRatio(true);
    imageView.setSmooth(true);
    imageView.setCache(true);

    return imageView;
  }

  /**
   * Method that creates and returns a label.
   *
   * @param labelName The text that the label consist of
   * @param fontSize The size of the font
   * @return a label
   */
  private Label createLabel(String labelName, int fontSize) {
    Label label = new Label(labelName);
    label.setFont(Font.font(design.getTheFont(), fontSize));
    label.setStyle("-fx-text-fill: " + Design.colourAsStyleString(design.getLabelTextColour()));
    return label;
  }

  /**
   * Method that creates a text field with a given width.
   *
   * @return a textfield
   */
  private TextField createTextField() {
    TextField textField = new TextField();
    textField.setPrefWidth(190.0);

    return textField;
  }
}
