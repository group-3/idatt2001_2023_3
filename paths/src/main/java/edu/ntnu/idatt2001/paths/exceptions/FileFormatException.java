package edu.ntnu.idatt2001.paths.exceptions;

public class FileFormatException extends Exception {
  public FileFormatException(String message) {
    super(message);
  }
}
