package edu.ntnu.idatt2001.paths.fileHandling;

import edu.ntnu.idatt2001.paths.story.Story;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class that writes to file.
 *
 * @author Adele and Maria
 */
public class StoryFileWriter {
  /**
   * Method that writes to file.
   *
   * @param file the file to write in
   * @param text text that is supposed to be written
   */
  public void writeToFile(File file, String text) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      writer.write(text);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Writes a given story to an existing or new file.
   *
   * @param file The file that the Story gets written to.
   * @param story A Story that gets written to the file.
   */
  public void writeStoryToFile(File file, Story story) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      writer.write(story.toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
