package edu.ntnu.idatt2001.paths.game;

import lombok.Getter;

/**
 * @param storyName the name of the story
 * @param storyPath the path of the storyFile
 * @param deadLinks the amount of deadLinks in the file
 */
public record GameFileInfo(
    @Getter String storyName, @Getter String storyPath, @Getter int deadLinks
) {
}
