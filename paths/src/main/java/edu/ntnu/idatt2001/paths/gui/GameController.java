package edu.ntnu.idatt2001.paths.gui;

import edu.ntnu.idatt2001.paths.actions.Action;
import edu.ntnu.idatt2001.paths.exceptions.ItemMissingException;
import edu.ntnu.idatt2001.paths.exceptions.LinkMissingException;
import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Game;
import edu.ntnu.idatt2001.paths.game.GameFileInfo;
import edu.ntnu.idatt2001.paths.game.Player;
import edu.ntnu.idatt2001.paths.goals.Goal;
import edu.ntnu.idatt2001.paths.story.Link;
import edu.ntnu.idatt2001.paths.story.Passage;
import edu.ntnu.idatt2001.paths.story.Story;
import java.util.ArrayList;
import java.util.List;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Controller for the Game part of Paths-application, creates the UI for playing
 * a Game based on a Story.
 *
 * @author Adele and Maria
 */
public class GameController {
  private final Scene gameScene;
  private HBox playerInfoPane;
  private HBox playerNamePane;
  private GridPane passageTextPane;
  private HBox linksPane;
  private HBox currentFilePane;
  private HBox toolsPane;
  private StackPane passagePane;
  private TextField playerNameField;
  private TextField currentHealthField;
  private TextField currentGoldField;
  private TextField currentScoreField;
  private TextArea inventoryField;
  private TextArea passageText;

  private final Design design;
  private final ArrayList<Button> linkButtons;
  private final ArrayList<Link> linksChosen;

  private final Player gamePlayer;
  private final Story story;
  private Passage currentPassage;
  private final Stage primaryStage;
  private final List<Goal> goals;
  private final GameFileInfo gameFileInfo;
  private final Label errorLabel;
  private final Button previousPassageButton;
  private final Button restartGameButton;

  /**
   * Constructor for a new GameController with a Game, Stage and GameFileInfo..
   *
   * @param game Represents a Game.
   * @param primaryStage Represents a Stage.
   * @param gameFileInfo Represents GameFileInfo with the game file's info.
   */
  public GameController(Game game, Stage primaryStage,  GameFileInfo gameFileInfo) {
    this.gamePlayer = game.getPlayer();
    this.story = game.getStory();
    this.goals = game.getGoals();
    this.primaryStage = primaryStage;
    this.gameFileInfo = gameFileInfo;
    currentPassage = story.getOpeningPassage();
    design = new Design();
    linksChosen = new ArrayList<>();
    linkButtons = new ArrayList<>();
    errorLabel = createErrorLabel();
    previousPassageButton = createButtonWithImage("images/icons/leftArrow.png", 20);
    restartGameButton = createButtonWithImage("images/icons/restart.png", 20);


    placePassageInfo();
    placePlayerInfo();
    updatePlayerInfo();
    placeGameInfo();
    placeGameOptions();

    AnchorPane gamePane = new AnchorPane(playerInfoPane, playerNamePane, passagePane, linksPane,
        currentFilePane, toolsPane);
    gamePane.setStyle(styleBackgroundColour(design.getSceneBackground()));

    gameScene = new Scene(gamePane, 800, 500);
    playGame();
  }


  /**
   * Retrieves and returns the controller's scene.
   *
   * @return A Scene representing the Game's scene.
   */
  public Scene getGameScene() {
    return gameScene;
  }

  /**
   * Creates, designs and places Nodes connected to the Player's info. Uses
   * {@link #createLabel(String)}, {@link #addIcon(String, int)}, {@link #createTooltip(String)},
   * {@link #createTextField()}, {@link #createTextArea(double, double, int)},
   * {@link Design#getTooltipSize()}, {@link #inventoryAsString()}.
   */
  private void placePlayerInfo() {
    final Label playerNameLabel = createLabel("Player name: ");
    final ImageView currentHealthIcon = addIcon("images/icons/heart.png", 30);
    final ImageView currentGoldIcon = addIcon("images/icons/coin.png", 30);
    final ImageView currentScoreIcon = addIcon("images/icons/shield.png", 25);
    final ImageView inventoryLabel = addIcon("images/icons/backpack.png", 30);
    Tooltip.install(currentHealthIcon, createTooltip("Current health"));
    Tooltip.install(currentGoldIcon, createTooltip("Current gold"));
    Tooltip.install(currentScoreIcon, createTooltip("Current score"));
    Tooltip.install(inventoryLabel, createTooltip("Current inventory"));

    currentScoreField = createTextField();
    currentGoldField = createTextField();
    currentHealthField = createTextField();
    playerNameField = createTextField();
    inventoryField = createTextArea(100, 120, design.getTooltipSize());
    inventoryField.setText(inventoryAsString());

    playerInfoPane = new HBox(5);
    playerNamePane = new HBox(5);

    playerNamePane.getChildren().addAll(playerNameLabel, playerNameField);
    playerInfoPane.getChildren().addAll(currentHealthIcon, currentHealthField,
        currentGoldIcon, currentGoldField, currentScoreIcon, currentScoreField,
        inventoryLabel, inventoryField);

    AnchorPane.setTopAnchor(playerNamePane, 10D);
    AnchorPane.setLeftAnchor(playerNamePane, 20D);
    AnchorPane.setTopAnchor(playerInfoPane, 10D);
    AnchorPane.setRightAnchor(playerInfoPane, 20D);

    playerNameField.setMaxWidth(120);
    currentHealthField.setMaxWidth(40);
    currentGoldField.setMaxWidth(40);
    currentScoreField.setMaxWidth(40);
  }

  /**
   * Creates and places the Panes that contain the current Passage and
   * Links. Uses {@link #createTextArea(double, double, int)},
   * {@link Design#getNormalTextSize()}.
   */
  private void placePassageInfo() {
    passageTextPane = new GridPane();
    linksPane = new HBox(5);
    passagePane = new StackPane();

    passageText = createTextArea(250, 500, design.getNormalTextSize());

    passageTextPane.getChildren().add(passageText);

    AnchorPane.setBottomAnchor(passagePane, 130D);
    AnchorPane.setRightAnchor(passagePane, 150D);
    AnchorPane.setBottomAnchor(linksPane, 90D);
    AnchorPane.setRightAnchor(linksPane, 150D);
    passagePane.getChildren().add(passageTextPane);
  }

  /**
   * Places the game option tools on the page, uses {@link #createButtonWithImage(String, int)},
   * {@link #createTooltip(String)}, {@link #restart()}, {@link #goToPreviousPassage()},
   * {@link #helpPressed()}, {@link #goHome()}.
   */
  private void placeGameOptions() {
    toolsPane = new HBox(5);
    AnchorPane.setRightAnchor(toolsPane, 150D);
    AnchorPane.setTopAnchor(toolsPane, 75D);


    restartGameButton.setTooltip(createTooltip("Restart story"));

    restartGameButton.setOnAction(event -> restart());
    previousPassageButton.setTooltip(createTooltip("Back to previous passage"));
    previousPassageButton.setOnAction(actionEvent -> goToPreviousPassage());

    Button helpButton = createButtonWithImage("images/icons/help.png", 22);
    helpButton.setTooltip(createTooltip("Press for help!"));
    helpButton.setOnAction(helpEvent -> helpPressed());

    Button homeButton = createButtonWithImage("images/icons/home.png", 20);
    homeButton.setTooltip(createTooltip("Press to go to Home page!"));
    homeButton.setOnAction(homeEvent -> goHome());

    toolsPane.getChildren().addAll(
        previousPassageButton, helpButton,
        restartGameButton, homeButton);
  }

  /**
   * Alert that pops up with information about how to use the application,
   * is triggered when help button is pressed.
   */
  private void helpPressed() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("How to play the story");
    alert.setHeaderText(null);
    alert.setContentText("""
        How to play:\s
        
        You are now on the game page. The current passage's content
        will be displayed in the text area and you will be able to choose
        what to do next. The buttons below the text area will lead you
        to new passages. If you don't understand the symbols on the
        page, hover over them for a bit and a description will appear.
        
        Good luck on your adventure!
                                       """);
    alert.setGraphic(addIcon("images/icons/help.png", 40));
    alert.showAndWait();
  }

  /**
   * Places the Nodes that are connect to the game information, uses
   * {@link #createLabel(String)}, {@link #createTooltip(String)},
   * {@link #createTextField()}.
   */
  private void placeGameInfo() {
    currentFilePane = new HBox();
    AnchorPane.setBottomAnchor(currentFilePane, 30D);
    AnchorPane.setLeftAnchor(currentFilePane, 20D);

    Label fileInfo = createLabel("\nFile name: "
        + gameFileInfo.storyName() + " \nPath: " + gameFileInfo.storyPath()
        + "\nDead links: " + gameFileInfo.deadLinks());
    fileInfo.setFont(Font.font(design.getTheFont(), 10));

    currentFilePane.getChildren().addAll(fileInfo, errorLabel);
  }

  /**
   * PLays the game, uses {@link #showPassage(Passage)}.
   */
  private void playGame() {
    linksPane.getChildren().removeAll(linkButtons);
    linkButtons.clear();
    passagePane.getChildren().clear();
    passagePane.getChildren().add(passageTextPane);
    showPassage(currentPassage);
  }

  /**
   * Shows the Passage in the method's parameter, uses
   * {@link Passage#getContent()}, {@link #placeLinks(Passage)}.
   *
   * @param passage The Passage that gets shown.
   */
  private void showPassage(Passage passage) {
    typeText(passage);
    passagePane.getChildren().add(findImageForPassage(passage));
  }

  /**
   * Places a Passage's Links in the Game scene, uses
   * {@link Passage#getLinks()}, {@link Passage#hasLinks()},
   * {@link Story#getPassage(Link)}, {@link Player#getHealth()},
   * {@link Link#getText()}, {@link Link#getActions()}, {@link Action#execute(Player)},
   * {@link #createButtonWithText(String)}, {@link #updatePlayerInfo()}, {@link #playGame()},
   * {@link #playLastPassage(Link)}.
   *
   * @param passage The Passage that links get fetched from.
   */
  private void placeLinks(Passage passage) {
    passage.getLinks().forEach(link -> {
      Button button = createButtonWithText(link.getText());
      linkButtons.add(button);
      linksPane.getChildren().add(button);
      button.setOnAction(event -> {
        try {
          linksChosen.add(link);
          currentPassage = story.getPassage(link);
          for (Action action : link.getActions()) {
            action.execute(gamePlayer);
            updatePlayerInfo();
          }
          if (currentPassage.hasLinks() && (gamePlayer.getHealth() > 0)) {
            playGame();
          } else {
            playLastPassage(link);
          }
        } catch (LinkMissingException | PlayerMissingException | ItemMissingException e) {
          errorLabel.setText(e.getMessage());
        }
      });
    });
  }

  /**
   * Fetches and updates the Player info displayed in the Game scene, uses
   * {@link Player#getName()}, {@link Player#getHealth()}, {@link Player#getGold()},
   * {@link Player#getScore()}, {@link #inventoryAsString()}.
   */
  private void updatePlayerInfo() {
    playerNameField.setText(gamePlayer.getName());
    currentHealthField.setText(Integer.toString(gamePlayer.getHealth()));
    currentGoldField.setText(Integer.toString(gamePlayer.getGold()));
    currentScoreField.setText(Integer.toString(gamePlayer.getScore()));
    inventoryField.setText(inventoryAsString());
  }

  /**
   * Checks and returns which Goals have been achieved in String format, uses
   * {@link Game#getGoals()}, {@link Goal#isFulfilled(Player)}.
   *
   * @return A String representing the Goals that were achieved.
   */
  private String achievedGoals() {
    StringBuilder stringBuilder = new StringBuilder();
    for (Goal goal : goals) {
      if (goal.isFulfilled(gamePlayer)) {
        stringBuilder.append("   -  ").append(goal.prettyText()).append("\n");
      }
    }
    return String.valueOf(stringBuilder);
  }

  /**
   * PLays the last Passage in the Story, uses {@link Player#getHealth()},
   * {@link Story#getPassage(Link)}, {@link Passage#getContent()},
   * {@link #createButtonWithText(String)}, {@link #achievedGoals()},
   * {@link #restart()}, {@link #goHome()}.
   *
   * @param link Represents the Link that links to the last Passage.
   */
  private void playLastPassage(Link link) {
    linksPane.getChildren().removeAll(linkButtons);
    linkButtons.clear();
    passagePane.getChildren().clear();
    passagePane.getChildren().add(passageTextPane);

    StringBuilder stringBuilder = new StringBuilder();
    if (gamePlayer.getHealth() > 0) {
      try {
        stringBuilder.append(story.getPassage(link).getContent())
            .append("\n\n                         *************  The end  *************");
      } catch (LinkMissingException e) {
        errorLabel.setText("Cannot find passage with Link.");
      }
    } else {
      stringBuilder.append("""
          After a great attempt you sadly died, and so the adventure ends.

                                   *************  The end  *************""");
    }

    stringBuilder.append("\n\nGoals achieved:\n").append(achievedGoals());
    passageText.setText(String.valueOf(stringBuilder));

    Button playAgain = createButtonWithText("Play again?");
    linkButtons.add(playAgain);

    Button backHome = createButtonWithText("Back to starting page");
    linkButtons.add(backHome);

    linksPane.getChildren().addAll(linkButtons);
    playAgain.setOnAction(actionEvent -> restart());
    backHome.setOnAction(actionEvent -> goHome());
  }

  /**
   * Restarts the game, uses {@link Story#getOpeningPassage()},
   * {@link #revertActions(Link)}, {@link #playGame()}.
   */
  private void restart() {
    if (!currentPassage.equals(story.getOpeningPassage()) && !linksChosen.isEmpty()) {
      for (Link link : linksChosen) {
        revertActions(link);
      }
      linksChosen.clear();
      currentPassage = story.getOpeningPassage();
      playGame();
    }
  }

  /**
   * Changes the Scene to the Home page, uses {@link HomeController#getHomeScene()}.
   */
  private void goHome() {
    HomeController homeController = new HomeController(primaryStage);
    Scene homeScene = homeController.getHomeScene();
    primaryStage.setScene(homeScene);
  }

  /**
   * Goes to the previous Passage in the Story, uses {@link Story#getPassage(Link)},
   * {@link Story#getOpeningPassage()}, {@link #assistGoToPreviousPassage(Passage, Link)}.
   */
  private void goToPreviousPassage() {
    if ((!linksChosen.isEmpty()) && (linksChosen.size() > 1)) {
      Link currentLink = linksChosen.get(linksChosen.size() - 1);
      Link goBackLink = linksChosen.get(linksChosen.size() - 2);
      Passage goBackToPassage;
      try {
        goBackToPassage = story.getPassage(goBackLink);
      } catch (LinkMissingException e) {
        errorLabel.setText("Cannot go back, Link is missing.");
        return;
      }
      assistGoToPreviousPassage(goBackToPassage, currentLink);
    } else if (linksChosen.size() == 1) {
      Link currentLink = linksChosen.get(0);
      Passage goBackPassage = story.getOpeningPassage();

      assistGoToPreviousPassage(goBackPassage, currentLink);
    }
  }

  /**
   * Assist {@link #goToPreviousPassage()} in going back
   * to the previous Passage.
   *
   * @param goBackToPassage Represents the Passage to go back to.
   * @param currentLink Represents the Link that was last chosen.
   */
  private void assistGoToPreviousPassage(Passage goBackToPassage, Link currentLink) {
    showPassage(goBackToPassage);
    revertActions(currentLink);
    linksChosen.remove(currentLink);

    linksPane.getChildren().clear();
    linkButtons.clear();
  }

  /**
   * Reverts the actions that the last Link executed, uses {@link Link#getActions()},
   * {@link Action#revert(Player)}, {@link #updatePlayerInfo()}.
   *
   * @param link Represents the Link with Actions that are to be reverted.
   */
  private void revertActions(Link link) {
    List<Action> actions = link.getActions();
    actions.forEach(action -> {
      try {
        action.revert(gamePlayer);
      } catch (PlayerMissingException | ItemMissingException e) {
        errorLabel.setText(e.getMessage());
      }
    });
    updatePlayerInfo();
  }

  /**
   * Creates a list of prompts matching images saved in resources, and
   * returns an ImageView with an Image that matches the Passage's content.
   * Uses {@link Passage#getContent()}, {@link #addIcon(String, int)}.
   *
   * @param passage Represents a Passage.
   * @return An ImageView with a suitable Image.
   */
  private ImageView findImageForPassage(Passage passage) {
    ArrayList<String> prompts = new ArrayList<>();
    prompts.add("dragon");
    prompts.add("spider");
    prompts.add("swords");
    prompts.add("book");
    prompts.add("door");
    prompts.add("gate");
    for (String prompt : prompts) {
      if (passage.getContent().toLowerCase().contains(prompt)) {
        return addIcon("images/storyImages/" + prompt + ".png", 100);
      }
    }
    return addIcon("images/storyImages/swords.png", 100);
  }

  /**
   * Takes a Color and uses it to
   * make it compatible with styling a Node's background colour.
   *
   * @param colour A Color representing the colour to be styled.
   * @return A String representation of the background Color.
   */
  private String styleBackgroundColour(Color colour) {
    return ("-fx-background-color: ") + Design.colourAsStyleString(colour);
  }

  /**
   * Creates and returns the Player's inventory as a string compatible
   * with TextArea.
   *
   * @return A String representation of the Player's inventory.
   */
  private String inventoryAsString() {
    StringBuilder stringBuilder = new StringBuilder();
    gamePlayer.getInventory().forEach(item -> stringBuilder.append(item).append("\n"));
    return String.valueOf(stringBuilder);
  }

  /**
   * Creates, styles and returns a text field, uses {@link Design#getTheFont()},
   * {@link Design#getTextBackground()}, {@link Design#getTooltipSize()},
   * {@link #styleBackgroundColour(Color)}.
   *
   * @return A styled TextField.
   */
  private TextField createTextField() {
    TextField field = new TextField();
    field.setEditable(false);
    field.setMouseTransparent(true);
    field.setFocusTraversable(false);

    field.setStyle(styleBackgroundColour(design.getTextBackground()));
    field.setFont(Font.font(design.getTheFont(), design.getTooltipSize()));

    return field;
  }

  /**
   * Creates, styles and returns a label with text, uses {@link Design#getTheFont()},
   * {@link Design#getNormalTextSize()}, {@link Design#getLabelTextColour()}.
   *
   * @param text A String representing the text to be added to the label.
   * @return A styled Label with text.
   */
  private Label createLabel(String text) {
    Label label = new Label(text);
    label.setFont(Font.font(design.getTheFont(), design.getNormalTextSize()));
    label.setStyle("-fx-text-fill: " + Design.colourAsStyleString(design.getLabelTextColour()));
    return label;
  }

  /**
   * Creates, styles and returns a button with text, uses {@link Design#getTheFont()},
   * {@link Design#getNormalTextSize()}, {@link Design#getButtonBackground()},
   * {@link #styleBackgroundColour(Color)}.
   *
   * @param text A String representing the text to add to the button.
   * @return A styled Button with text.
   */
  private Button createButtonWithText(String text) {
    Button newButton = new Button(text);
    newButton.setFont(Font.font(design.getTheFont(), design.getNormalTextSize()));
    newButton.setStyle(styleBackgroundColour(design.getButtonBackground()));
    return newButton;
  }

  /**
   * Creates, styles and returns a button with an image on, uses
   * {@link Design#getButtonBackground()}, {@link #styleBackgroundColour(Color)}.
   *
   * @param imageInfo A String representing the image's reference.
   * @param fitWidth An integer representing the image's fit width.
   * @return A styled Button with an image and preferred fit width.
   */
  private Button createButtonWithImage(String imageInfo, int fitWidth) {
    Button newButton = new Button();
    newButton.setGraphic(addIcon(imageInfo, fitWidth));
    newButton.setStyle(styleBackgroundColour(design.getButtonBackground()));
    return newButton;
  }

  /**
   * Creates, styles and returns a text area, uses {@link Design#getTextBackground()},
   * {@link Design#getTheFont()}, {@link #styleBackgroundColour(Color)}.
   *
   * @param prefHeight A double representing the area's preferred height.
   * @param preWidth A double representing the area's preferred width.
   * @param textSize An integer representing the area's text size.
   * @return A TextArea with preferred height, width and text size.
   */
  private TextArea createTextArea(double prefHeight, double preWidth, int textSize) {
    TextArea textArea = new TextArea();
    textArea.setPrefSize(preWidth, prefHeight);
    textArea.setStyle(styleBackgroundColour(design.getTextBackground()));
    textArea.setFont(Font.font(design.getTheFont(), textSize));

    textArea.setEditable(false);
    textArea.setWrapText(true);
    textArea.setFocusTraversable(false);

    return textArea;
  }

  /**
   * Creates, styles and returns a tooltip with the String parameter as text. Uses
   * {@link #styleBackgroundColour(Color)}, {@link Design#getTheFont()},
   * {@link Design#getTooltipSize()}, {@link Design#getTooltipBackground()}.
   *
   * @param text A String representing the tooltip's text.
   * @return A Tooltip created from the parameters.
   */
  private Tooltip createTooltip(String text) {
    Tooltip tooltip = new Tooltip(text);
    tooltip.setFont(Font.font(design.getTheFont(), design.getTooltipSize()));
    tooltip.setStyle(styleBackgroundColour(design.getTooltipBackground())
        + ";" + "-fx-text-fill: black;");
    return tooltip;
  }

  /**
   * Creates, styles and returns an ImageView.
   *
   * @param imageInfo A string that represents the image reference.
   * @param fitWidth An integer representing the image's width.
   * @return An ImageView created from the parameters.
   */
  private ImageView addIcon(String imageInfo, int fitWidth) {
    Image image = new Image(imageInfo);
    ImageView imageView = new ImageView();
    imageView.setImage(image);
    imageView.setFitWidth(fitWidth);
    imageView.setPreserveRatio(true);
    imageView.setSmooth(true);
    imageView.setCache(true);

    return imageView;
  }

  /**
   * Styles the error label.
   */
  private Label createErrorLabel() {
    Label label = new Label("");
    label.setFont(Font.font(design.getTheFont(), 10));
    label.setStyle(Design.colourAsStyleString(Color.RED));
    return label;
  }


  /**
   * Types out the Passage taken inn as a parameter, uses {@link Passage#getContent()},
   * {@link #placeLinks(Passage)}.
   *
   * @param passage Represents the Passage that gets typed.
   */
  private void typeText(Passage passage) {
    Timeline timeline = new Timeline();
    previousPassageButton.setDisable(true);
    restartGameButton.setDisable(true);
    Button stopTyping = createButtonWithText("Skip typing");
    toolsPane.getChildren().add(stopTyping);

    String passageString = passage.getContent();
    final IntegerProperty counter = new SimpleIntegerProperty(0);

    stopTyping.setOnAction(stopEvent -> {
      timeline.stop();
      placeLinks(passage);
      passageText.setText(passage.getContent());
      toolsPane.getChildren().remove(stopTyping);
      previousPassageButton.setDisable(false);
      restartGameButton.setDisable(false);
    });

    KeyFrame keyFrame = new KeyFrame(
        Duration.seconds(0.05),
        typingEvent -> {
          if (counter.get() > passageString.length()) {
            timeline.stop();
            placeLinks(passage);
            toolsPane.getChildren().remove(stopTyping);
            previousPassageButton.setDisable(false);
            restartGameButton.setDisable(false);
          } else {
            passageText.setText(passageString.substring(0, counter.get()));
            counter.set(counter.get() + 1);
          }
        });
    timeline.getKeyFrames().add(keyFrame);
    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();
  }



}
