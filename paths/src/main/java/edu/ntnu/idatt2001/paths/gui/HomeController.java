package edu.ntnu.idatt2001.paths.gui;

import edu.ntnu.idatt2001.paths.fileHandling.StoryFileReader;
import edu.ntnu.idatt2001.paths.game.Game;
import edu.ntnu.idatt2001.paths.game.GameFileInfo;
import edu.ntnu.idatt2001.paths.game.Player;
import edu.ntnu.idatt2001.paths.goals.Goal;
import edu.ntnu.idatt2001.paths.story.Story;
import java.io.File;
import java.util.List;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Controller for the First scene of Paths-application.
 *
 * @author Adele and Maria
 */
public class HomeController {
  private final PlayerPane playerPane;
  private final GoalPane goalPane;
  private final GamePane gamePane;
  private final GameInfoPane gameInfoPane;
  private final AnchorPane anchorPane = new AnchorPane();
  private final Scene startScene;
  private final Button startGameButton = new Button("Start Game");
  private File storyFile;
  private static final String REGEX_FOR_NUMBERS = "\\d+";
  private GameController gameControllerClass;
  private Scene gameScene;
  private Story storyFromFile;
  private final Stage primaryStage;
  private final Design design;
  private String storyName = "";
  private String storyPath = "";
  private int deadLinks = 0;

  /**
   * The constructor of the class.
   *
   * @param primaryStage A Stage
   */
  public HomeController(Stage primaryStage) {
    this.primaryStage = primaryStage;
    design = new Design();

    BorderPane borderPane = new BorderPane();
    BorderPane leftBorderPane = new BorderPane();
    startGameButton.setFont(Font.font(design.getTheFont(), design.getNormalTextSize()));
    startGameButton.setStyle(("-fx-background-color: ")
        + Design.colourAsStyleString(design.getButtonBackground()));


    playerPane = new PlayerPane(design);
    gamePane = new GamePane(design);
    goalPane = new GoalPane(design);
    gameInfoPane = new GameInfoPane(design);

    String buttonInfo = "Press for help!";

    playerPane.playerHelpButton.setTooltip(createTooltip(buttonInfo));
    playerPane.playerHelpButton.setOnAction(helpEvent ->
        helpPressed("How to fill in player info", playerPane.getPlayerHelpButtonString()));

    gamePane.gameHelpButton.setTooltip(createTooltip(buttonInfo));
    gamePane.gameHelpButton.setOnAction(helpEvent ->
        helpPressed("How to start the game", gamePane.getGameHelpButtonString()));

    goalPane.goalHelpButton.setTooltip(createTooltip(buttonInfo));
    goalPane.goalHelpButton.setOnAction(helpEvent ->
        helpPressed("How to enter goals", goalPane.getGoalHelpButtonString()));


    Label title = new Label("Before the game begins");
    title.setFont(Font.font(design.getTheFont(), FontWeight.BOLD, design.getMainTitleSize()));
    title.setStyle("-fx-text-fill: " + Design.colourAsStyleString(design.getLabelTextColour()));

    BorderPane titleBorderPane = new BorderPane();
    BorderPane buttonBorderPane = new BorderPane();
    BorderPane gameInfoBorderPane = new BorderPane();
    BorderPane bottomPane = new BorderPane();

    titleBorderPane.setCenter(title);
    buttonBorderPane.setRight(startGameButton);
    gameInfoBorderPane.setLeft(gameInfoPane);
    bottomPane.setLeft(gameInfoBorderPane);
    bottomPane.setRight(buttonBorderPane);

    leftBorderPane.setTop(playerPane);
    leftBorderPane.setBottom(gamePane);

    leftBorderPane.setPadding(new Insets(10, 60, 10, 20));
    goalPane.setPadding(new Insets(10, 20, 10, 20));
    gamePane.setPadding(new Insets(30, 0, 0, 0));
    titleBorderPane.setPadding(new Insets(0, 0, 20, 0));


    borderPane.setTop(titleBorderPane);
    borderPane.setLeft(leftBorderPane);
    borderPane.setRight(goalPane);
    borderPane.setBottom(bottomPane);


    primaryStage.setTitle("A Story based game...");
    pressButtonToStartEvent();

    eventUploadFileButton();
    eventUseExistingStory();

    anchorPane.getChildren().addAll(borderPane);
    Group root = new Group();
    root.getChildren().addAll(anchorPane);

    sizingOfSite();

    startScene = new Scene(root, 800, 500, design.getSceneBackground());
    primaryStage.setScene(startScene);
    primaryStage.setResizable(false);
    primaryStage.show();

  }

  /**
   * Creates, styles and returns a tooltip with the String parameter as text. Uses
   * {@link #styleBackgroundColour(Color)}, {@link Design#getTheFont()},
   * {@link Design#getTooltipSize()}, {@link Design#getTooltipBackground()}.
   *
   * @param text A String representing the tooltip's text.
   * @return A Tooltip created from the parameters.
   */
  private Tooltip createTooltip(String text) {
    Tooltip tooltip = new Tooltip(text);
    tooltip.setFont(Font.font(design.getTheFont(), design.getTooltipSize()));
    tooltip.setStyle(styleBackgroundColour(design.getTooltipBackground())
        + ";" + "-fx-text-fill: black;");
    return tooltip;
  }

  /**
   * Takes a Color and uses it to
   * make it compatible with styling a Node's background colour.
   *
   * @param colour A Color representing the colour to be styled.
   * @return A String representation of the background Color.
   */
  private String styleBackgroundColour(Color colour) {
    return ("-fx-background-color: ") + Design.colourAsStyleString(colour);
  }

  /**
   * Alert that pops up with information about how to use the application,
   * is triggered when help button is pressed.
   */
  private void helpPressed(String title, String helpText) {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle(title);
    alert.setHeaderText(null);
    alert.setContentText(helpText);
    alert.setGraphic(addIcon("images/icons/help.png", 40));
    alert.showAndWait();
  }

  /**
   * Creates, styles and returns an ImageView.
   *
   * @param imageInfo A string that represents the image reference.
   * @param fitWidth An integer representing the image's width.
   * @return An ImageView created from the parameters.
   */
  private ImageView addIcon(String imageInfo, int fitWidth) {
    Image image = new Image(imageInfo);
    ImageView imageView = new ImageView();
    imageView.setImage(image);
    imageView.setFitWidth(fitWidth);
    imageView.setPreserveRatio(true);
    imageView.setSmooth(true);
    imageView.setCache(true);

    return imageView;
  }

  /**
   * Method to get the home Scene/first page.
   *
   * @return the first Scene
   */
  public Scene getHomeScene() {
    return startScene;
  }

  /**
   * Method that sets the size of the site.
   */
  private void sizingOfSite() {
    anchorPane.setMinSize(800, 500);
    anchorPane.setMaxSize(800, 500);
    anchorPane.setPrefSize(800, 500);
    anchorPane.setPadding(new Insets(10, 10, 10, 10));
  }

  /**
   * Method that handles click on start game button.
   *
   */
  private void pressButtonToStartEvent() {
    startGameButton.setOnAction(event -> {

      if (!checkPlayerInfoText()) {
        return;
      }

      if (storyFile == null) {
        return;
      }

      Player.PlayerBuilder playerBuilder = new Player.PlayerBuilder(playerPane.getPlayerNameField().getText())
          .health(Integer.parseInt(playerPane.getPlayerHealthField().getText()))
          .gold(Integer.parseInt(playerPane.getPlayerGoldField().getText()));

      Player player = playerBuilder.build();
      List<Goal> goals = goalPane.getGoals();
      GameFileInfo gameFileInfo = new GameFileInfo(storyName, storyPath, deadLinks);

      Game game = new Game(player, storyFromFile, goals);

      gameControllerClass = new GameController(game, primaryStage, gameFileInfo);
      gameScene = gameControllerClass.getGameScene();
      primaryStage.setScene(gameScene);
    });
  }

  /**
   * Method that handles action on the upload file button.
   *
   */
  private void eventUploadFileButton() {
    Button fileButton = gamePane.getFileButton();
    FileChooser fileChooser = new FileChooser();
    StoryFileReader storyFileReader = new StoryFileReader();
    fileButton.setOnAction(event -> {
      gamePane.getErrorMsg().setText("");
      storyFile = fileChooser.showOpenDialog(primaryStage);
      if (storyFile != null) {
        if (storyFile.getAbsolutePath().endsWith(".paths")) {
          fileButton.setText(storyFile.getName());
          try {
            storyFromFile = storyFileReader.createStoryFromFile(storyFile);
            fillStoryInfo();
          } catch (Exception e) {
            gamePane.getErrorMsg().setText("File is in the wrong format");
          }
        } else {
          fileButton.setText("File needs '.paths' ending");
          storyFile = null;
        }
      } else {
        fileButton.setText("Please upload file");
      }
    });
  }

  /**
   * Method that handles action on the combobox where you can choose existing stories.
   *
   */
  private void eventUseExistingStory() {
    ComboBox<String> preDefinedStories = gamePane.getPreDefinedStories();
    StoryFileReader storyFileReader = new StoryFileReader();

    preDefinedStories.setOnAction(event -> {
      gamePane.getErrorMsg().setText("");
      String selectedStory = preDefinedStories.getValue();

      storyFile = new File("src/main/resources/stories/" + selectedStory + ".paths");

      if (storyFile.exists()) {
        try {
          storyFromFile = storyFileReader.createStoryFromFile(storyFile);
          fillStoryInfo();
        } catch (Exception e) {
          gamePane.getErrorMsg().setText("An error occurred, try again!");
        }
      } else {
        gamePane.getErrorMsg().setText("An error occurred with the story!");
        storyFile = null;
      }
    });
  }

  /**
   * Method that fills labels with information about the story.
   *
   */
  private void fillStoryInfo() {
    storyName = storyFile.getName();
    storyPath = storyFile.getPath();
    deadLinks = storyFromFile.getBrokenLinks().size();

    gameInfoPane.getStoryNameLabel().setText("Story name: ");
    gameInfoPane.getStoryName().setText(storyName);
    gameInfoPane.getStoryPathlabel().setText("Path to story: ");
    gameInfoPane.getPath().setText(storyPath);
    gameInfoPane.getDeadLinksLabel().setText("Dead links: ");
    gameInfoPane.getDeadLinks().setText(String.valueOf(deadLinks));
  }

  /**
   * Method that checks player info fields.
   *
   * @return true if the input is legal and false if its illegal
   *
   */
  private boolean checkPlayerInfoText() {
    boolean allGood = true;

    if (playerPane.getPlayerNameField().getText().isEmpty()) {
      playerPane.getPlayerNameField().setPromptText("Please enter a name");
    }

    if (!checkGoldField()) {
      playerPane.getPlayerGoldField().clear();
      playerPane.getPlayerGoldField().setPromptText("Please write a positive number");
      allGood = false;
    }

    if (!checkHealthField()) {
      playerPane.getPlayerHealthField().clear();
      playerPane.getPlayerHealthField().setPromptText("Please write a positive number");
      allGood = false;
    }

    return allGood;
  }

  /**
   * Method that checks if gold field is legal.
   *
   * @return true if the input is legal and false if its illegal
   *
   */
  private boolean checkGoldField() {
    return playerPane.getPlayerGoldField().getText().matches(REGEX_FOR_NUMBERS);
  }

  /**
   * Method that checks if health field is legal.
   *
   * @return true if the input is legal and false if its illegal
   *
   */
  private boolean checkHealthField() {
    return playerPane.getPlayerHealthField().getText()
        .matches(REGEX_FOR_NUMBERS) && !isHealthZero();
  }

  /**
   * Method that checks if health is zero.
   *
   * @return true if the health is zero and false if it's not
   *
   */
  private boolean isHealthZero() {
    return playerPane.getPlayerHealthField().getText().matches("0*$");
  }

}