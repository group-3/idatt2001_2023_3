package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.ItemMissingException;
import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;

/**
 * InventoryAction implements the Action interface and is
 * an action that changes the player's inventory.
 *
 * @author Adele and Maria
 */
public class InventoryAction implements Action {
  //expand with being able to lose items from inventory?
  private final String item;

  public InventoryAction(String item) {
    this.item = item;
  }

  /**
   * Adds an item to the Player's inventory using {@link Player#addToInventory(String)}.
   *
   * @param player Represents a Player.
   * @throws PlayerMissingException If the Player is null.
   * @throws ItemMissingException If the Item is null.
   */
  @Override
  public void execute(Player player) throws PlayerMissingException, ItemMissingException {
      if (player == null) {
        throw new PlayerMissingException("Player is null");
      }
    player.addToInventory(this.item);
  }

  /**
   * Reverts the inventory action by removing the item from the Player's inventory,
   * uses {@link Player#getInventory()} and {@link Player#removeFromInventory(String)}.
   *
   * @param player Represents a Player.
   * @throws PlayerMissingException If the player is null.
   * @throws ItemMissingException If the item is null or not in the inventory.
   */
  @Override
  public void revert(Player player) throws PlayerMissingException, ItemMissingException {
    if (player == null) {
      throw new PlayerMissingException("Player is null");
    }
    player.removeFromInventory(item);
  }

  /**
   * Returns the Action as a String in a file-friendly format.
   *
   * @return A String representation of the Action.
   */
  @Override
  public String toString() {
    return ("{Inventory}" + "(" + item + ")");
  }
}
