package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.game.Player;
import java.util.ArrayList;
import java.util.List;

/**
 * InventoryGoal implements the Goal interface and represents an inventory
 * goal that is achieved by having a collection of mandatory items in one's
 * inventory.
 *
 * @author Adele and Maria
 */
public class InventoryGoal implements Goal {
  private final List<String> mandatoryItems;

  /**
   * Creates an inventory goal with a list of mandatory items as a parameter.
   *
   * @param mandatoryItems A List of items representing items one has to have to
   *                       achieve the inventory goal.
   */
  public InventoryGoal(List<String> mandatoryItems) {
    //should we check parameter here? Can a goal have an empty list as a goal?
    this.mandatoryItems = mandatoryItems;
  }

  /**
   * The constructor of the class.
   *
   */
  public InventoryGoal() {
    this.mandatoryItems = new ArrayList<>();
  }

  /**
   * Method that adds a mandatoryItem to the list of mandatoryItems.
   *
   * @param mandatoryItem the item thats going to be added to the list
   *
   */
  public void addMandatoryItem(String mandatoryItem) {
    if (!mandatoryItems.contains(mandatoryItem)) {
      mandatoryItems.add(mandatoryItem);
    }
  }

  /**
   * Checks if the inventory goal for a player has been achieved by using
   * {@link Player#getInventory()} to compare the player's inventory with
   * the mandatory items needed to achieve the inventory Goal.
   *
   * @param player Represents a player.
   * @return A boolean representing if the inventory goal was achieved.
   */
  @Override
  public boolean isFulfilled(Player player) {
    List<String> playerInventory = player.getInventory();
    for (String mandatoryItem : mandatoryItems) {
      if (!(playerInventory.contains(mandatoryItem))) {
        return false;
      }
    }
    return true;
  }

  /**
   * Creates and returns a string representation of the Goal.
   *
   * @return A string representation of the Goal.
   */
  @Override
  public String prettyText() {
    String text = String.join("\n  * ", mandatoryItems);
    return "Mandatory items: \n  * " + text;
  }
}
