package edu.ntnu.idatt2001.paths.actions;

import edu.ntnu.idatt2001.paths.exceptions.ItemMissingException;
import edu.ntnu.idatt2001.paths.exceptions.PlayerMissingException;
import edu.ntnu.idatt2001.paths.game.Player;

/**
 * Interface Action that represents actions that happen
 * to players in the game.
 *
 * @author Adele and Maria
 */
public interface Action {
  /**
   * Executes the action on the player.
   *
   * @param player Represents a Player.
   * @throws PlayerMissingException If the player parameter is null.
   */
  void execute(Player player) throws PlayerMissingException, ItemMissingException;

  /**
   * Reverts the action on the player.
   *
   * @param player Represents a Player.
   */
  void revert(Player player) throws PlayerMissingException, ItemMissingException;
}
