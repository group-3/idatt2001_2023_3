package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.game.Player;

/**
 * ScoreGoal implements the Goal interface and represents a score goal
 * that is achieved by reaching a minimum amount of points.
 *
 * @author Adele and Maria
 */
public class ScoreGoal implements Goal {
  private final int minimumPoints;

  /**
   * Creates a score goal with a minimum amount of points to reach as a parameter.
   *
   * @param minimumPoints An integer representing a minimum amount of points the player has
   *                      to reach to achieve the health goal.
   * @throws IllegalArgumentException If the minimum amount of points is negative.
   */
  public ScoreGoal(int minimumPoints) {
    if (minimumPoints < 0) {
      throw new IllegalArgumentException("Minimum points to achieve goal cannot be negative.");
    }
    this.minimumPoints = minimumPoints;
  }

  /**
   * Checks if the score goal for a player has been achieved by using {@link Player#getScore()}
   * to compare the player's health with the minimum points.
   *
   * @param player Represents a Player.
   * @return A boolean representing if the score goal was achieved.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return (player.getScore() >= minimumPoints);
  }

  /**
   * Creates and returns a string representation of the Goal.
   *
   * @return A string representation of the Goal.
   */
  @Override
  public String prettyText() {
    return "Score: " + minimumPoints;
  }
}
