package edu.ntnu.idatt2001.paths.goals;

/**
 * Class that represents types of goals.
 *
 * @author Adele and Maria
 *
 */
public class GoalTypes {

  /**
   * An empty private constructor to hide the public one, makes it not possible to instantiate.
   * And can only be used as static class.
   */
  private GoalTypes() {

  }

  public static final String HEALTH = "Health";
  public static final String GOLD = "Gold";
  public static final String INVENTORY = "Inventory";
  public static final String SCORE = "Score";


}
