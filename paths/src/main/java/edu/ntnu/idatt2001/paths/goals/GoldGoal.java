package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.game.Player;

/**
 * GoldGoal implements the Goal interface and represents a goal
 * that gets reached by acquiring a minimum amount of gold.
 *
 * @author Adele and Maria
 */
public class GoldGoal implements Goal {
  private final int minimumGold;

  /**
   * Creates the gold goal with a minimum amount of gold to reach as parameter.
   *
   * @param minimumGold An integer representing a minimum amount of gold the player has to acquire.
   * @throws IllegalArgumentException If the minimum amount of gold is negative.
   */
  public GoldGoal(int minimumGold) {
    if (minimumGold < 0) {
      throw new IllegalArgumentException("Minimum gold for goal cannot be negative.");
    }
    this.minimumGold = minimumGold;
  }

  /**
   * Checks if a player has achieved the gold goal by comparing the player's gold
   * with the minimum amount of gold. Uses {@link Player#getGold()} to compare.
   *
   * @param player Represents a player.
   * @return A boolean representing if the gold Goal has been achieved.
   */
  @Override
  public boolean isFulfilled(Player player) {
    return (player.getGold() >= minimumGold);
  }

  /**
   * Creates and returns a string representation of the Goal.
   *
   * @return A string representation of the Goal.
   */
  @Override
  public String prettyText() {
    return "Gold: " + minimumGold;
  }
}
