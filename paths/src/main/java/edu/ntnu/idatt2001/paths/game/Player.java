package edu.ntnu.idatt2001.paths.game;

import edu.ntnu.idatt2001.paths.exceptions.ItemMissingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a player.
 *
 * @author Adele and Maria
 */
public class Player {

  private final String name;
  private int health;
  private int score;
  private int gold;
  private final List<String> inventory;

  /**
   * Constructor for a new Player that's built by a builder.
   *
   * @param builder Builder object for the class
   *
   */
  private Player(PlayerBuilder builder) {
    this.name = builder.name;
    this.health = builder.health;
    this.gold = builder.gold;
    this.score = builder.score;
    this.inventory = builder.inventory;
  }

  /**
   * This method sets the player's score.
   *
   * @param score the players score of the game as an int
   *
   */
  public void setScore(int score) {
    this.score = score;
  }

  /**
   * This method sets the player's gold.
   *
   * @param gold the amount of gold the player has as an int
   *
   */
  public void setGold(int gold) {
    this.gold = gold;
  }

  /**
   * This method sets the player's health.
   *
   * @param health the players health as an int
   *
   */
  public void setHealth(int health) {
    this.health = health;
  }

  /**
   * This method gets the player's name.
   *
   * @return the name of the player as a string
   *
   */
  public String getName() {
    return name;
  }

  /**
   * This method gets the player's health.
   *
   * @return the players health as an int
   *
   */
  public int getHealth() {
    return health;
  }

  /**
   * This method gets the player's score.
   *
   * @return the players score as an int
   *
   */
  public int getScore() {
    return score;
  }

  /**
   * This method gets the player's gold.
   *
   * @return the players gold as an int
   *
   */
  public int getGold() {
    return gold;
  }

  /**
   * This method gets a list of all the items in the player's inventory.
   *
   * @return a list of all the items in the player's inventory
   *
   */
  public List<String> getInventory() {
    return inventory;
  }

  /**
   * This method adds a given number to the player's health.
   *
   * @param health the amount of health that's going to be added to the player's health.
   *
   */
  public void addHealth(int health) {
    this.health += health;
  }

  /**
   * This method adds a given number to the player's score.
   *
   * @param points the number of points that's going to be added to the player's score as an int
   *
   */
  public void addScore(int points) {
    this.score += points;
  }

  /**
   * This method adds a given number of gold to the player's gold.
   *
   * @param gold the number of gold that's going to be added to the player's gold as an int.
   *
   */
  public void addGold(int gold) {
    this.gold += gold;
  }

  /**
   * This method adds a given item as a string to the player's inventory.
   *
   * @param item the item that's going to be added to the player's inventory as a String
   * @throws ItemMissingException if there is no item
   */
  public void addToInventory(String item) throws ItemMissingException {
    if (item == null) {
      throw new ItemMissingException("Item can't be null");
    }
    
    inventory.add(item);
  }

  /**
   * Method that removes an item from the inventory.
   *
   * @param item item to remove
   * @throws ItemMissingException if there is no item
   */
  public void removeFromInventory(String item) throws ItemMissingException {
    if (item == null) {
      throw new ItemMissingException("Item cannot be null");
    }
    if (inventory.contains(item)) {
      inventory.remove(item);
    } else {
      throw new ItemMissingException("Item is not in inventory and therefore cannot be removed");
    }
  }

  /**
   * Builder class for player.
   *
   * @author Adele and Maria
   */
  public static class PlayerBuilder {
    private final String name;
    private int health = 0;
    private int score = 0;
    private int gold = 0;
    private List<String> inventory = new ArrayList<>();

    public PlayerBuilder(String name) {
      this.name = name;
    }

    /**
     * Sets the health of a new instance.
     *
     * @param health given number of health
     * @return new Instance with the given number as health
     *
     */
    public PlayerBuilder health(int health) {
      this.health = health;
      return this;
    }

    /**
     * Sets the score of a new instance.
     *
     * @param score given number of score
     * @return new instance with the given number as score
     *
     */
    public PlayerBuilder score(int score) {
      this.score = score;
      return this;
    }

    /**
     * Sets the gold of a new instance.
     *
     * @param gold given number of gold
     * @return new instance with the given number as gold
     *
     */
    public PlayerBuilder gold(int gold) {
      this.gold = gold;
      return this;
    }

    /**
     * Sets the inventory of a new instance.
     *
     * @param inventory a list of items in the inventory
     * @return new instance with the list as inventory
     *
     */
    public PlayerBuilder inventory(List<String> inventory) {
      this.inventory = inventory;
      return this;
    }

    /**
     * Makes a new instance of Player.
     *
     * @return A new Instance
     */
    public Player build() {
      return new Player(this);
    }

  }

}
